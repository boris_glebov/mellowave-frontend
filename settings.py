# -*- coding: utf-8 -*-
__author__ = 'Boris'

import os
from ConfigParser import RawConfigParser

PROJECT_DIR = os.path.abspath(os.path.dirname(__file__))

config = RawConfigParser()
config.read('{0}/settings.ini'.format(PROJECT_DIR))

#
# Версия
#
version = "0.1.0"

#
# ОБЩИЕ НАСТРОЙКИ ПРИЛОЖЕНИЯ
#

# [APPLICATION]

# Если приложение запущено в режиме разработки, выставить в True
IS_DEBUG = config.getboolean('APPLICATION', 'debug')
SECRET_KEY = config.get('APPLICATION', 'secret_key')

# [LOGGER]
LOG_PATH = config.get('LOGGER', 'directory')

# [DATABASE]
MONGO_SERVER = config.get('DATABASE', 'host')
MONGO_PORT = config.getint('DATABASE', 'port')
MONGO_DATABASE_NAME = config.get('DATABASE', 'db_name')
MONGO_METABASE_NAME = config.get('DATABASE', 'metadb_name')
MONGO_REPLICASET = config.get('DATABASE', 'replicaset')

# [VKONTAKTE]
API_ID = config.get('VKONTAKTE', 'api_id')
API_SECRET = config.get('VKONTAKTE', 'api_secret')

#
# [LAST.FM]
#
LASTFM_API_KEY = config.get('LASTFM', 'api_key')
LASTFM_API_SECRET = config.get('LASTFM', 'api_secret')

#
# [MEMCACHED]
#
MEMCACHED_SERVER = config.get('MEMCACHED', 'host')
MEMCACHED_PORT = config.get('MEMCACHED', 'port')

#
# LIQUIDSOAP (Telnet)
#
LIQUIDSOAP_SERVER = config.get('LIQUIDSOAP', 'host')
LIQUIDSOAP_PORT = config.getint('LIQUIDSOAP', 'port')

