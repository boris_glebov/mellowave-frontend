# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

from mongokit import *
import settings

db = Connection(u"{0}:{1}".format(settings.MONGO_SERVER, settings.MONGO_PORT))


class Entity(Document):
    __database__ = settings.MONGO_METABASE_NAME

    use_dot_notation = True
    use_autorefs = True

    @property
    def id(self):
        return self._id


@db.register
class UsersEntity(Entity):
    __collection__ = "users"

    structure = {
        'user_id': unicode,
        'name': {
            'first_name': unicode,
            'last_name': unicode
        },

        'sex': IS(u'male', u'femaile'),
        'avatar': unicode
    }


class StatisticEntity(Entity):
    __collection__ = "statistics"

    structure = {
        'user_id': unicode,
        'station_id': unicode,
        'date': datetime.datetime,
        'type': IS(u'favorite_station_event',
                   u'come_on_channel_event',
                   u'favorite_track_event',
                   u'block_track_event',
                   u'favorite_station_event',
                   u'track_scrobble_event',
                   u'person_station_empty')
    }

@db.register
class GenreEntity(Entity):
    __collection__ = "genres"

    structure = {
        'name': unicode,
        'search_name': unicode
    }

