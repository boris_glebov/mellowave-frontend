# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

from mongokit import *


class ArtistEntityMigration(DocumentMigration):
    def migration01__remove_tracks_field(self):
        self.target = {'tracks': {'$exists': True}}
        self.update = {'$unset': {"tracks": ""}}