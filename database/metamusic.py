# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

import settings

from mm_migration import *
from datetime import datetime
from mongokit import *


db = Connection(u"{0}:{1}".format(u'localhost', settings.MONGO_PORT))


class Entity(Document):
    __database__ = settings.MONGO_METABASE_NAME

    use_dot_notation = True
    use_autorefs = True

    @property
    def id(self):
        return self._id

    def get_safe(self, key, default=None):
        last_node = self
        for node in key.split('.'):
            try:
                last_node = last_node.get(node)
            except:
                last_node = default
                break

        return default if last_node == self or last_node is None else last_node


@db.register
class ArtistEntity(Entity):
    __collection__ = "artists"

    structure = {
        'name': unicode,
        'sort_name': unicode,
        'country': unicode,
        'hero_image': unicode,

        'mb': {
            'id': unicode,
            'name': int,
            'sort_name': int,
            'last_update': datetime
        },

        'lastfm': {
            'url': unicode,
            'wiki': {
                'bio': unicode,
                'short_bio': unicode,
                'url': unicode
            },
            'listeners': int,
            'plays': int
        },

        'tags': [unicode],
        'images': [{
            'url': unicode,
            'width': int,
            'height': int
        }]
    }

    migration_handler = ArtistEntityMigration
    required_fields = ['name', 'sort_name']
    default_values = {
        'country': u'',
        'hero_image': u'',
        'tags': [],
        'images': []
    }

    #
    # Properties

    @property
    def tracks(self):
        return db.TrackEntity.find({'artist.$id': self.id})


@db.register
class ReleaseEntity(Entity):
    __collection__ = "release"

    structure = {
        'name': unicode,
    }

    required_fields = ['name']

    #
    # Properties

    @property
    def tracks(self):
        return db.TrackEntity.find({'album': self})


@db.register
class TrackEntity(Entity):
    __collection__ = "tracks"

    structure = {
        'name': unicode,
        'search_name': unicode,
        'duration': int,
        'position': int,

        'album': ReleaseEntity,
        'artist': ArtistEntity,

        'mb': {
            'id': unicode,
            'name': int,
            'pid': int,
            'track_pid': int,
            'last_update': datetime,
        }
    }

    indexes = [
        {
            'fields': 'artist',
            'unique': False
        },
        {
            'fields': 'album',
            'unique': False
        }]

    required_fields = ['name', 'search_name', 'artist']
    default_values = {
        'duration': 0,
        'position': 0
    }


















