# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

from logger import logger
from database.metamusic import db

info = logger.info
error = logger.error

if __name__ == '__main__':
    import model as m
    from asq.initiators import query

    for artist in m.db.ArtistEntity.find(timeout=False):

        info(u"=====================================================================\n")
        info(artist["name"])

        newArtist = db.ArtistEntity()

        newArtist.name = artist['name']
        newArtist.sort_name = artist['sort_name']
        newArtist.country = artist['country']
        newArtist.hero_image = artist.get_safe('hero_image', u'')

        newArtist.mb = {
            'id': artist.get_safe("mbid", None),
            'name': artist.get_safe("mb_name", -1),
            'sort_name': artist.get_safe('mb_sort_name', -1),
            'last_update': artist.get_safe('mb_last_update')
        }

        newArtist.tags = artist.get_safe('tags', [])

        # сохраняем исполнителя:
        try:
            newArtist.validate()
            newArtist.save()

        except Exception as ex:
            error(u'Error: {0}'.format(ex.message), ex)

            continue

        albums = []

        last_album_name = None
        for t in artist.get_safe('tracks', []):
            album_name = t['release']

            if last_album_name != album_name:
                last_album_name = album_name
                info(u'\t{0}'.format(album_name))

            album = query(albums).where(lambda a: a['name'] == album_name).first_or_default(None)

            if album is None:
                album = db.ReleaseEntity()
                album.name = album_name

                try:
                    album.validate()
                    album.save()

                    albums.append(album)

                except Exception as ex:
                    error(u'Error: {0}'.format(ex.message), ex)

            info(u'\t\t- {0}'.format(t['name']))

            track = db.TrackEntity()

            track.name = t['name']
            track.search_name = t['search_name']
            track.duration = t['duration']
            track.position = t['position']

            track.album = album
            track.artist = newArtist

            track.mb = {
                'id': t['mbid'],
                'name': t['mb_name'],
                'pid': t['mb_pid'],
                'track_pid': t['mb_track_pid'],
            }

            try:
                track.validate()
                track.save()
            except Exception as ex:
                error(u'Error: {0}'.format(ex.message), ex)