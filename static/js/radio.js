/**
 * Created with PyCharm.
 * User: Glebov Boris
 * Date: 19.03.13
 * Time: 23:13
 */

/*
 Prototypes
 */

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};


/* Global vars*/

var session = {
    current_song: {
        track_id : 'undefined'
    },
    current_artist: undefined,
    station_url: station_url,
    station_id: station_id
}

/*
Custom Types
*/

/*Перечисление: состояния страницы*/
function StatePageEnum(){
    this.RadioStream = "expert";
    this.RadioAuthor = "artist";
}

/*Класс представляющий ViewModel страницы*/
function RadioViewModel()
{
    var self = this;

    this.history = ko.observableArray([]);

    this.current_page_state = ko.observable(StatePage.RadioStream);

    this.is_busy = ko.observable(false);

    /* Биография */
    this.bio = ko.observable('');

    /* Тэги */
    this.tags = ko.observable([]);

    /* Связанные исполнители */
    this.similar_artists = ko.observableArray([]);

    this.station_name = ko.observable('');

    /* Функции по работе с плеером */
    this.player = ko.observable(new RadioPlayer());

    /* Утилиты */
    this.utils = ko.observable(new Utils());

    /* Основные функции */
    this.commons = ko.observable(new Commons());

    /* Уведомления */
    this.notice = ko.observable(new Notice());

    //VK
    this.vk_wrapper = ko.observable(new VKWrapper());

    //Echonest

    this.echonest_wrapper = ko.observable(new EchonestWrapper());

    /* Methods */

    this.init = function(){
        self.player().init();
    };

    this.rename_station = function(){
        if (station_type != 'personal' && station_type != 'genre'){
            return;
        }

        this.utils().show_change_station_name_dialog();
    };

    this.save_station_name = function(){
        if (station_type != 'personal' && station_type != 'genre'){
            return;
        }

        $.ajax({
            url:'/api/radio/rename_personal_station/' + station_id + '/station_name/' + self.station_name()
        }).done(function(response){

            var r = ViewModel.utils().get_response_as_object(response);

                if(r.error){
                    ViewModel.utils().show_dialog_message('Ошибка', r.error);
                    return;
                }

            $('#station-name').text(self.station_name());
        });
    };

    this.create_radio_by_genre = function(){
        var settings = {artists:[this.name]};

        create_stream_by_genre(settings);
    };

    this.get_next_track = function(cb){
        console.log('start get_next_track');

        $.ajax({
            url: '/api/radio/get_next_track/' + station_id,
            dataType:'json'
        }).done(function(response){
                var r = ViewModel.utils().get_response_as_object(response);

                if(r.error){
                    console.log(r.error);

                    ViewModel.utils().show_dialog_message('Ошибка', r.error, function(){
                        if(r.code == 1) //Список пуст
                            window.history.go(-1);
                    });

                    return;
                }

                // Устанавлиаем биографию
               self.bio(r.bio);

               // Сохраняем тэги во временную переменную.
               // Для того чтобы показать их на страницы нужно вызвать метод
               // set_tags. Сейчас этого не делаем, так как нужно показать биографию
               session.tags = r.tags;

               session.current_artist = r.artist;
               session.current_song = {
                   track_id: r.mbid,
                   track_name: r.artist + ' - ' + r.title
               };

               // Обновляем счетчик лайков:
               ViewModel.commons()._set_likes(r.track_info.is_user_like, r.track_info.likes);
               ViewModel.commons()._set_bans(r.track_info.is_user_ban);

               $('#SearchOtherSongs').attr('href','http://vk.com/search?&c[section]=audio&c[q]=' + r.artist);

                console.log('done get_next_track');
                console.log(r);

                if(cb){
                    console.log('get_next_track запускаем функцию обратного вызова');
                    cb(r);
                }else{
                    console.log("get_next_track функция обратного вызова не опредлена!!!")
                }
        });
    };

    this.next_track_play = function(){
        console.log('start next_track_play');

        ViewModel.utils().show_busy();

        self.get_next_track(function(track){
            self.vk_wrapper().search_track(track, self.play_track);
        });
    };

    this.get_similar_artists = function(artist_name){
        console.log('Получаем список связанных исполнителей.');

        $.ajax({
            url: '/api/radio/get_similar_artits/' + artist_name
        }).done(function(response){
            var r = ViewModel.utils().get_response_as_object(response);

            console.log(r);

            ViewModel.similar_artists(r);
        });
    };

    this.play_track = function(track){
        if(!track){
            ViewModel.utils().hide_busy();
            ViewModel.next_track_play();
            return;
        }

        console.log(track);

        var title = track.artist + ' - ' + track.title;
        ViewModel.add_history_track(self.create_history_track(title));

        $("#info-block").fadeOut('slow', function(){
            self.player().title(title);
            self.commons().set_bio(ViewModel.bio());
            self.commons().set_tags(session.tags);

            ViewModel.echonest_wrapper().fetch_images(track.artist);

            ViewModel.utils().hide_busy();
            $(this).fadeIn('slow')
        });

        // Получим список связанных исполнителей:
        self.get_similar_artists(track.artist);

        // Устанавливаем трек в статус
        VK.api("audio.setBroadcast", {
            audio: track.owner_id + '_' + track.aid,
        });

        self.player().play(track.url);

        // Запомним играющий трэк ВК
        session.track_vk = track;
    };

    /* История */

    this.add_history_track = function(track){
        if(self.history().length == 10)
        {
            self.history.pop();
        }

        self.history.unshift(track);
    };

    this.get_history = function(){
        $.ajax({
            url:'/api/radio/get_history_songs/' + station_id
        }).done(function(response){
                var r = self.utils().get_response_as_object(response);

                $.each(r, function(i, track){
                    self.add_history_track(self.create_history_track(track));
                });

                // Получаем начальное состояние канала:
                _getStat();
            });
    };

    this.create_history_track = function(track_name){
        return  {
            track_name: track_name,
            search_url: "http://vk.com/search?&c[section]=audio&c[q]=" + track_name
        }
    };

    this.is_expert = function(){
        return station_type == StatePage.RadioStream;
    }
}


/* Класс для управления плеером */
var RadioPlayer = function(){
    var self = this;

    /* Properties */

    this.is_play = ko.observable(true);
    this.is_volume = ko.observable(true);
    this.vol = ko.observable(1);
    this.is_mute = ko.observable(false);
    this.jplayer = ko.observable(undefined);

    this.title = ko.observable('');

    /* Methods */

    this.init = function(){
        var solution = "flash";

        self.jplayer($("#jquery_jplayer_1").jPlayer({
            ready: function (event) {
                if(ViewModel.is_expert()){
                    self.play(self.generate_stream());
                }else{
                   ViewModel.next_track_play();
                }
            },
            solution:solution,
            swfPath: "/static/jslib/jplayer/Jplayer.swf",
            supplied: "mp3, oga",
            oggSupport: true,
            nativeSupport:true,
            preload:true,
            error: function (event) {
                console.log(event.jPlayer.error);
                console.log(event.jPlayer.error.type);

                ViewModel.next_track_play();
            },
            ended: function () {
                console.log('end track.')
                if(ViewModel.is_expert())
                    return;

                ViewModel.next_track_play();
            }
        }));

        // Настраиваем регулятор громкости:
        $("#volume_slider").on("changed", function(e, val){
            self.vol(val / 100);

            self.volume(self.vol());
        });
    }

    this.play = function(source){

        var Stream = {
            title: session.station_id,
            mp3: source
        };

        $(self.jplayer()).jPlayer("setMedia", Stream);
        $(self.jplayer()).jPlayer("play");
    }

    this.ff = function(){
        ViewModel.next_track_play();
    }

    this.pause = function(){
        if(ViewModel.is_expert())
            $(self.jplayer()).jPlayer('clearMedia');
        else
            $(self.jplayer()).jPlayer('pause');
    }

    this.play_pause = function(){
        if(self.is_play())
        {
            ViewModel.utils().replace_image_src('pause_white.png','play_white.png', '.play-pause-control');
            self.pause();
        }
        else
        {
            ViewModel.utils().replace_image_src('play_white.png','pause_white.png', '.play-pause-control');

            if(ViewModel.is_expert())
                self.play(self.generate_stream());
            else
            {
                $(self.jplayer()).jPlayer('play');
            }
        }

        self.is_play(!self.is_play());
    }

    this.volume = function(volume){
        $(self.jplayer()).jPlayer("volume", volume);
        $('#volume_slider').data('slider').val(volume * 100)
    }

    this.volume_off = function(){
        self.volume_on_off(true);
    }

    this.volume_on = function(){
        self.volume_on_off(false);
    }

    this.volume_on_off = function(is_mute_off){
        if(self.is_volume() && is_mute_off)
        {
            self.is_volume(false);
            self.volume(0);
        }
        else if(!self.is_volume() && !is_mute_off)
        {
            self.is_volume(true);
            self.volume(self.vol());
        }
    }

    this.generate_stream = function(){
        return  session.station_url + session.station_id + ".mp3";
    }
}

/* Общие функции */
var Commons = function(){

    var self = this;

    /* Properties */

    this.likes = ko.observable(0);

    /* Public methods */

    this.like = function(){
        // Если информация по трэку еще не получено, его нельзя лайкать
        if(!session.current_song){
            return;
        }

        ViewModel.utils().show_busy();

        $.ajax({
            url:'/api/radio/add_favorite_song/station/' + session.station_id + '/song/' + session.current_song.track_id
            + '/artist/' + session.current_artist + '/title/' + session.current_song.track_name
        }).done(function(response){
                ViewModel.utils().hide_busy();

                r = ViewModel.utils().get_response_as_object(response);

                if(!r){
                        console.log('Error add song to favorite.')
                    return;
                }else if(r.error){
                    console.log(r.message);
                    return
                }

                var delta = r.is_like ? 1 : -1;
                var currentLikesCount = self.likes() //parseInt($("#track_like_counter").text());

                currentLikesCount += delta;

                self._set_likes(r.is_like, currentLikesCount);

                if(r.is_like)
                    self._set_bans(false);
            });
    }

    this.unlike = function(){

        ViewModel.utils().show_busy();

        $.ajax({
            url:'/api/radio/ban_song/station/' + session.station_id + '/song/' + session.current_song.track_id
        }).done(function(response){
                ViewModel.utils().hide_busy();

                r = ViewModel.utils().get_response_as_object(response);

                // Если информация по трэку еще не получено, его нельзя лайкать
                if(!session.current_song){
                    return;
                }

                if(!r){
                    console.log('Error ban song.')
                    return;
                }else if(r.error){
                    console.log(r.message);
                    return
                }

                self._set_bans(r.is_ban);

                if(r.is_ban){
                    self._set_likes(false);

                    // Если станция находится в режиме исполнителя, то
                    // перематываем заблокированный трэк.
                    if(!ViewModel.is_expert()){
                        ViewModel.next_track_play();
                    }
                }
            });
    }

    this.favorite_station = function(){
        $.ajax({
            url:'/api/radio/add_favorite_station/' + session.station_id + '/station_type/' + station_type
        }).done(function(response){
                var r = ViewModel.utils().get_response_as_object(response);

                if(r.response_type == "favorite" && r.status == true)
                {
                    // Делаем звездочку черной:
                    $('#station-favorite-icon').removeClass('icon-star');
                    $('#station-favorite-icon').addClass('icon-star-3');

                    ViewModel.notice().show_notice(ViewModel.notice().generate_success_message('Станция успешно добавлена  в избранные.',
                        'Список всех ваших любимых станций вы можете увидеть <a href="/radio" style="color: white;"><u>на главной странице</u></a>.'));

                }else if(r.response_type == "unfavorite" && r.status == true) {
                    // Делаем звездочку белой:
                    $('#station-favorite-icon').removeClass('icon-star-3');
                    $('#station-favorite-icon').addClass('icon-star');

                    ViewModel.notice().show_notice(ViewModel.notice().generate_success_message('Станция успешно убрана  из избранных.',
                        'Список всех ваших любимых станций вы можете увидеть <a href="/radio" style="color: white;"><u>на главной странице</u></a>.'));
                }
            });
    };

    this.show_listeners = function(){
        $.ajax({
            url: '/api/radio/get_listeners/' + session.station_id,
            success: function(response){
                $.Dialog({
                    'title'       : 'Вы слушаете станцию вместе с ...',
                    'content'     : response,
                    'draggable'   : false,
                    'overlay'     : true,
                    'closeButton' : true,
                    'buttonsAlign': 'right',
                    'position'    : {
                        'zone'    : 'custom'
                    },
                    X : 70,
                    Y : 100,
                    Width: 800,
                    'buttons'     : {
                        'OK'     :
                        {
                            'action': function(){}
                        }
                    }
                });
            }
        });
    };

    this.start_radio_by_artist = function(artist_name){

        ViewModel.utils().show_busy();

        $.ajax({
            url:'/api/radio/create_person_station/by/name',
            type: "POST",
            data: JSON.stringify({
                artists:[artist_name instanceof RadioViewModel ? session.current_artist : artist_name]
            }),
            contentType: 'application/json;charset=UTF-8'
        }).done(function(response){

                ViewModel.utils().hide_busy();

                r = ViewModel.utils().get_response_as_object(response);

                if(r == undefined || r.error || !r.station_id){
                    console.log(r.error);

                    ViewModel.utils().show_dialog_message('Техническая неполадка',
                        "Нам не удалось подготовить для вас станцию :(");
                    return;
                }

                document.location.href = '/radio/by/station/' + r.station_id;
            });
    }

    this.start_radio_by_similar_artist = function(artist){
        self.start_radio_by_artist(artist.name);
    }

    this.add_song_to_playlist = function(){
        if(!ViewModel.is_expert() && session.track_vk)
        {
            ViewModel.vk_wrapper().add_song_to_playlist(session.track_vk.aid, session.track_vk.owner_id);
            return;
        }


        if(!session.current_song)
            return;

        $.ajax({
            url:'/api/radio/get_track_vk_id/station/' + session.station_id + '/track/' + session.current_song.track_id
        }).done(function(response){
                r = ViewModel.utils().get_response_as_object(response);

                if(r.error)
                {
                    ViewModel.notice().show_add_song_to_playlist_error();
                    return;
                }

                ViewModel.vk_wrapper().add_song_to_playlist(r.track_id, r.owner_id);
            });
    }

    this.set_bio = function(bio){

       $('.wiki-text').html(bio);

       //$('#SearchOtherSongs').attr('href','http://vk.com/search?&c[section]=audio&c[q]=' + r.artist);

       ViewModel.utils().replace_links_on_lastfm();
    }

    this.set_tags = function(tags){
        ViewModel.tags(tags);
    }

    /* Private methods */

    this._set_likes = function(is_like, likes){

        self.likes(likes);

        if(likes == undefined || likes <= 0)
            $("#track_like_counter").text('');
        else
            $("#track_like_counter").text(likes);

        if(is_like)
            ViewModel.utils().replace_image_src('heart_white.png','heart_white_red.png', '#like_img');
        else
            ViewModel.utils().replace_image_src('heart_white_red.png','heart_white.png', '#like_img');
    }

    this._set_bans = function(is_ban)
    {
        if(is_ban)
            ViewModel.utils().replace_image_src('unlike_white.png','unlike_white_red.png', '#unlike_img');
        else
            ViewModel.utils().replace_image_src('unlike_white_red.png','unlike_white.png', '#unlike_img');
    }
}

/* Служебные утилиты */
var Utils = function()
{
    var self = this;

    this.replace_image_src = function(replace_from, replace_to, control_id)
    {
        $(control_id).attr('src', $(control_id) .attr('src').replace(replace_from, replace_to));
    }

    this.show_dialog_message = function(title, content, cb){
        $.Dialog({
            'title'      : title,
            'content'    : content,
            'buttonsAlign': 'right',
            'position'    : {
                'zone'    : 'custom'
            },
            X : 200,
            Y : 160,
            Width : 400,
            'buttons'    : {
                'Закрыть'    : {
                    'action': function() {
                        if(cb){
                           cb();
                        }
                    }
                }
            }
        });
    };

    this.show_change_station_name_dialog = function(){
        $.Dialog({
            'title'      : "Смена названия станции",
            'content'    : ViewModel.notice().generate_input_dialog(),
            'buttonsAlign': 'right',
            'position'    : {
                'zone'    : 'custom'
            },
            X : 200,
            Y : 160,
            Width : 400,
            'buttons'    : {
                'Сохранить': {
                    'action':function(){
                        ViewModel.station_name($('#station-name-dlg').val());

                        ViewModel.save_station_name();
                    }
                },
                'Закрыть'    : {
                    'action': function() {}
                }
            }
        });
    };

    this.show_busy = function(){
        ViewModel.is_busy(true);
        $('#busy-indicator').show();
    };

    this.hide_busy = function(){
        ViewModel.is_busy(false);
        $('#busy-indicator').hide();
    };

    this.get_random_arbitary = function(min, max){
        return Math.round(Math.random() * (max - min) + min);
    };

    this.go_url = function(url){
        window.open(url, '_blank');
    };

    this.get_response_as_object = function(response){
        /*В хроме response приходит ввиде десериализованного объекты, в некоторых браузеров все же приходит
         * просто plain-текст. */
        if(typeof response == 'string')
        {
            r = $.parseJSON(response);
        }else{
            r = response;
        }

        return r;
    }

    this.replace_links_on_lastfm = function(){
        // Добавляем к ссылке 'подробнее на ласт.фм' атрибут target="_blank"
        var links = $('.wiki-text > a');

        if(links.length > 0)
        {
            var more_on_lastfm_link =  links[links.length - 1];
            $(more_on_lastfm_link).attr("target", '_blank');
            $(more_on_lastfm_link).text("Узнать больше о " + session.current_artist + " на last.fm");
        }
    }
}

var Notice = function()
{
    var self = this;

    this.generate_fault_message = function(header, message){
        return '<div class="bg-color-red">' +
            '<a href="#" class="close" onclick="NoticeClose()"></a>' +
            '<div class="notice-header fg-color-yellow">' + header + '</div>' +
            '<div class="notice-text">' + message  + '</div>' +
            '</div>'
    };

    this.generate_success_message = function(header, message){
        return '<div class="bg-color-green">' +
            '<a href="#" class="close" onclick="NoticeClose()"></a>' +
            '<div class="notice-header fg-color-white">' + header + '</div>' +
            '<div class="notice-text">' + message + '</div>' +
            '</div>'
    };

    this.generate_input_dialog = function(){
        var html =
            '<h2>Имя станции:</h2>' +
                '<div class="input-control text"><input type="text" autofocus="" id="station-name-dlg" value="' + $('#station-name-full').val() +'"></div>';

        return html;
    };

    this.show_add_song_to_playlist_success = function(){
        ViewModel.notice().show_notice(ViewModel.notice().generate_success_message('Аудио запись успешно добавлена в Вашу библиотеку',
            'Для просмотра вашей музыкальной библиотеки перейдите <a href="http://vk.com/audio" target="_blank" style="color: white;"><u>по ссылке</u></a>.'));
    };

    this.show_add_song_to_playlist_error = function(){
        ViewModel.notice().show_notice(ViewModel.notice().generate_fault_message('Внутренняя ошибка приложения',
            'При попытки добавления трэка произошла ошибка, попробуйте эту функцию позже. Список ваших записей вы можете просмотреть <a href="http://vk.com/audio" target="_blank" style="color: white;"><u>по ссылке</u></a>.'));
    };

    this.show_notice = function(html){
        $("#notice").html(html);
        $("#notice").attr('display', 'block');

        $("#notice").fadeIn('slow', function(){
            setTimeout(function(){
                self.hide_notice();
            }, 5000);
        });
    }

    this.hide_notice = function(){
        $("#notice").fadeOut('slow');
    }
}

var VKWrapper = function(){
    var self = this;

    this.search_track = function(recording, cb) {
        console.log('search 1. start search track in VK.');

        VK.api("audio.search", {
            q: recording.artist + ' - ' + recording.title,
            auto_complete: 1,
            sort: 2,
            count: 10
        }, function (d) {

            var data = ViewModel.utils().get_response_as_object(d);

            console.log('search 2. start callback success search_track');

            if (!data || !data.response) {
                //Скорее всего ошибка.
                console.log(data);

                ViewModel.next_track_play();

                return;
            }

            if (data.response.length < 2)
            {
                console.log("VK не нашел трэк по запросу. Берем следующую песню.")
                ViewModel.next_track_play();

                return;
            }

            var track = undefined;
            for (idx in data.response){
                track = data.response[idx];

                if(track.artist == recording.artist && track.title == recording.title){
                    console.log("Нашли нужный трэк. Запускаем его.");
                    if(cb){
                        cb(track);
                    }

                    return;
                }
            }

            // ВК нашел трэки, но они не удовлетворяют нашим требованиям.
            // В пилотной версии попробуем брать первый, посмотрим на сколько все будет плохо.
            if(data.response.length > 1){
                console.log("Берем самый первый");

                var first = data.response[1];
                first.artist = recording.artist;

                if(cb){
                    cb(first);
                }

                return;
            }

            ViewModel.next_track_play();
        });
    }

    this.add_song_to_playlist = function(aid, owner_id)
    {
        VK.api("audio.add", {aid: aid, oid: owner_id }, function(data){
            if(data.response)
            {
                ViewModel.notice().show_add_song_to_playlist_success();
            }
            else
            {
                ViewModel.notice().show_add_song_to_playlist_error();
            }
        });
    }
};

var EchonestWrapper = function(){
    this.fetch_images = function(artist) {
        var url = 'http://developer.echonest.com/api/v4/artist/images';

        var args = {
            format:'json',
            api_key: '7HZ6UZEPZH5VKVJ1W',
            name: artist,
            results: 100
        };

        console.log("Fetching images for " + artist);

        $.getJSON(url, args,
            function(data) {
                if (! ('images' in data.response)) {
                    console.log("Can't find any images for " + artist);
                    _setSlides();
                } else {
                    var images = [];
                    $.each(data.response.images, function(index, item) {
                        images.push(item.url);
                    });

                    session.slides = images;
                    _setSlides(images, 0);
                }
            },
            function() {
                console.log("Trouble getting blog posts for " + artist);
                _setSlides();
            }
        );
    }
};

/* Create singleton instance */

var StatePage = new StatePageEnum();
var ViewModel = new RadioViewModel();



/***/

//Publish and Global
$(function () {

    ViewModel.init();

    if(station_type == StatePage.RadioStream)
    {
        // Ajax
        setInterval(_getStat, 3000);

        //Загружаем плейлист:
        ViewModel.get_history();
    }
    else
    {

    }

    // Local
    setInterval(_getRandomImage, 30000);

    ko.applyBindings(ViewModel);
});


function NoticeClose(){
    ViewModel.notice().hide_notice();
}

/*
* Служебные функции
*/

var _last_show_image_url = '';
var brokeImage = [];


/*
* Получает информацию о текущем трэке, биографию артиста, изображения для слайд шоу.
*/
function _getStat()
{
    // Если радио на паузе, то не обновляем статистику
    if(!ViewModel.player().is_play())
        return;

    $.ajax({
        url:'/api/radio/get_station_stat/' + station_id + '/now_playing/' + session.current_song.track_id,
        dataType:'json'
    }).done(function(response)
        {
            var r = ViewModel.utils().get_response_as_object(response);

            // Кол-во слушателей
            $('.listener-counter').text(r.listener_count);

            if(r.is_short)
                return;

            // Обновляем счетчик лайков:
            ViewModel.commons()._set_likes(r.track_info.is_user_like, r.track_info.likes);
            ViewModel.commons()._set_bans(r.track_info.is_user_ban);

            if(session.current_song == undefined || r.current_song.track_id != session.current_song.track_id)
            {
                session.current_song = r.current_song;
                session.current_artist = r.artist;
                session.slides = r.slides;

                // Получаем связанных исполнителей:
                ViewModel.get_similar_artists(session.current_artist);

                // Добавляем трэк в историю:
                ViewModel.add_history_track(ViewModel.create_history_track(session.current_song.track_name));

                brokeImage = [];
                _setSlides(r.slides, 0);

                var _response = r;

                $('#info-block').fadeOut('slow', function(){

                    // Получаем тэги
                    ViewModel.tags(r.tags);

                    session.current_song = _response.current_song;
                    ViewModel.player().title(_response.current_song.track_name);

                    if(_response.bio != undefined && _response.bio != ''){
                        $('.wiki-text').html(_response.bio);

                        ViewModel.bio(_response.bio);
                    }

                    $('#SearchOtherSongs').attr('href','http://vk.com/search?&c[section]=audio&c[q]=' + r.artist);

                    ViewModel.utils().replace_links_on_lastfm();

                   $('#info-block').fadeIn('slow');
                });
            }
        });
}

function _getRandomImage()
{
    if(!ViewModel.player().is_play() || session.slides == undefined){
        return;
    }

    _setSlides(session.slides, ViewModel.utils().get_random_arbitary(0, session.slides.length - 1))
}

function _setSlides(slides, slide_index)
{
   var _this = $('#slides');

   if(slides == undefined || slides.length == 0)
   {
       $('#slide').attr('src', '/static/img/lastfm.png');
       return;
   }

    var img_url = slides[slide_index];

    if(img_url == _last_show_image_url)
    {
        return;
    }

    var div_width = 500;
    var div_height = 300;

    var proxyObj = new Image();
    proxyObj.src = img_url;
    proxyObj.onload = function()
    {
        if(this.width >= div_width && this.height >= div_height)
        {
            // Меняем картинку:
            _this.fadeOut('slow', function()
            {
                $('#slide').attr('src', img_url);

                _this.fadeIn('slow');
            });

        }else {
            _getRandomImage();
        }

        _last_show_image_url = img_url;
    }
}





