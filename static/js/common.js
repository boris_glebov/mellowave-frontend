/**
 * Created with PyCharm.
 * User: Boris
 * Date: 27.06.13
 * Time: 22:09
 * Набор общих функций
 */

var Utils = function(){
        this.is_busy = false;

        this.show_dialog_message = function(title, content){
        $.Dialog({
            'title'      : title,
            'content'    : content,
            'buttonsAlign': 'right',
            'position'    : {
                'zone'    : 'custom',
            },
            X : 200,
            Y : 160,
            Width : 400,
            'buttons'    : {
                'Закрыть'    : {
                    'action': function() {}
                },
            }
        });
    }

    this.get_response_as_object = function (response){
        /*В хроме response приходит ввиде десериализованного объекты, в некоторых браузеров все же приходит
         * просто plain-текст. */
        if(typeof response == 'string')
        {
            r = $.parseJSON(response);
        }else{
            r = response;
        }

        return r;
    }

    this.show_busy = function(){
        this.is_busy = true;
        $('#busy-indicator').show();
    }

    this.hide_busy = function(){
        this.is_busy = false;
        $('#busy-indicator').hide();
    }
}

function create_favorite_station(station_id)
{
    var utils = new Utils();

    if(utils.is_busy){
        return;
    }

    utils.show_busy();

    $.ajax({
        url:'/api/radio/create_fav_person_station/' + station_id,
    }).done(function(response){
            utils.hide_busy();

            r = utils.get_response_as_object(response);

            if(r == undefined || r.error || !r.station_id){
                    utils.show_dialog_message('Попробуй позже ...',
                    "Нам не удалось подготовить для вас станцию :(");

                return;
            }

            document.location.href = '/radio/by/station/' + r.station_id;
        });
}

function create_stream_by_genre(settings){
    var utils = new Utils();

    if(utils.is_busy){
        return;
    }

    utils.show_busy();

    $.ajax({
        url:'/api/radio/create_person_station/by/genre',
        type: "POST",
        data: JSON.stringify(settings),
        contentType: 'application/json;charset=UTF-8'
    }).done(function(response){
            utils.hide_busy();

            r = utils.get_response_as_object(response);

            if(r == undefined || r.error || !r.station_id){
                utils.show_dialog_message('Попробуй позже ...',
                    "Нам не удалось подготовить для вас станцию :(");

                return;
            }

            document.location.href = '/radio/by/station/' + r.station_id;
        });
}

function create_stream_by_favorite(){
    var utils = new Utils();

    if(utils.is_busy){
        return;
    }

    utils.show_busy();

    $.ajax({
        url:'/api/radio/create_person_station/by/favorite',
        type: "GET",
        contentType: 'application/json;charset=UTF-8'
    }).done(function(response){
            utils.hide_busy();

            r = utils.get_response_as_object(response);

            if(r == undefined || r.error || !r.station_id){
                if(r.code == 1){
                    utils.show_dialog_message('Не удалось подготовить станцию',
                        "Слишком мало материала для радио рекомендаций. Отметьте минимум 10 трэков.");
                    return;
                }
                utils.show_dialog_message('Попробуй позже ...',
                    "Нам не удалось подготовить для вас станцию :(");

                return;
            }

            document.location.href = '/radio/by/station/' + r.station_id;
        });
}

function create_stream (obj){
    var utils = new Utils();

    if(utils.is_busy){
        return;
    }

    utils.show_busy();

    $.ajax({
        url:'/api/radio/create_person_station/by/name',
        type: "POST",
        data: JSON.stringify(obj),
        contentType: 'application/json;charset=UTF-8'
    }).done(function(response){
           utils.hide_busy();

            r = utils.get_response_as_object(response);

            if(r == undefined || r.error || !r.station_id){
                utils.show_dialog_message('Попробуй позже ...',
                    "Нам не удалось подготовить для вас станцию :(");

                return;
            }

            document.location.href = '/radio/by/station/' + r.station_id;
        });
}

function create_stream_by_mbid(obj){

    var utils = new Utils();

    if(utils.is_busy){
        return;
    }

    utils.show_busy();

    $.ajax({
        url:'/api/radio/create_person_station/by/mbid',
        type: "POST",
        data: JSON.stringify(obj),
        contentType: 'application/json;charset=UTF-8'
    }).done(function(response){
            utils.hide_busy();

            $("#busy-indicator").hide();

            r = utils.get_response_as_object(response);

            if(r == undefined || r.error || !r.station_id){
                utils.show_dialog_message('Попробуй позже ...',
                    "Нам не удалось подготовить для вас станцию :(");

                return;
            }

            document.location.href = '/radio/by/station/' + r.station_id;
        });
}

$(function(){
    _checkWhereOpenSite();

    VK.callMethod('scrollWindow', 0, 0);
    VK.callMethod('resizeWindow', 960, document.body.clientHeight);
})


function _checkWhereOpenSite(){
    if(window.parent.frames.length == 0){
        //document.location = 'http://vk.com/app2333169';
    }
}

