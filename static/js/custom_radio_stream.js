/**
 * Created with PyCharm.
 * User: Boris
 * Date: 13.06.13
 * Time: 21:49
 * To change this template use File | Settings | File Templates.
 */


var CustomStreamViewModel = function(){

    var self = this;

    //Коллекция элементов из которых будет составлен плейлист (пр.: "Кукриниксы", "ДДТ", "Я свободен!")
    self.SelectedQueryItems = ko.observableArray([]);

    self.AutocompleateQueryItems = ko.observableArray([]);

    self.IsMixRecomendation = ko.observable(false);

    this.create_stream = function(obj){
        $.ajax({
            url:'/api/radio/create_person_station/by/name',
            type: "POST",
            data: JSON.stringify(obj),
            contentType: 'application/json;charset=UTF-8'
        }).done(function(response){
               r = self.utils().get_response_as_object(response);

                if(r == undefined || r.error || !r.station_id){
                    self.utils().show_dialog_message('Попробуй позже ...',
                        "Нам не удалось подготовить для вас станцию :(");

                    return;
                }

                document.location.href = '/radio/by/station/' + r.station_id;
            });
    };

    this.create_stream_by_mbid = function(obj){

        $("#busy-indicator").show();

        $.ajax({
            url:'/api/radio/create_person_station/by/mbid',
            type: "POST",
            data: JSON.stringify(obj),
            contentType: 'application/json;charset=UTF-8'
        }).done(function(response){

                $("#busy-indicator").hide();

                r = self.utils().get_response_as_object(response);

                if(r == undefined || r.error || !r.station_id){
                    self.utils().show_dialog_message('Попробуй позже ...',
                        "Нам не удалось подготовить для вас станцию :(");

                    return;
                }

                document.location.href = '/radio/by/station/' + r.station_id;
            });
    };

    this.utils = ko.observable(new Utils());
};

var Utils = function(){
    this.get_response_as_object = function(response){
        /*В хроме response приходит ввиде десериализованного объекты, в некоторых браузеров все же приходит
         * просто plain-текст. */
        if(typeof response == 'string')
        {
            r = $.parseJSON(response);
        }else{
            r = response;
        }

        return r;
    };

    this.show_dialog_message = function(title, content){
        $.Dialog({
            'title'      : title,
            'content'    : content,
            'buttonsAlign': 'right',
            'position'    : {
                'zone'    : 'custom'
            },
            X : 200,
            Y : 160,
            Width : 400,
            'buttons'    : {
                'Закрыть'    : {
                    'action': function() {}
                }
            }
        });
    };

    this.show_setup_dialog = function(){


        $.Dialog({
            'title'      : "Настрой свой эфир",
            'content'    : content,
            'buttonsAlign': 'right',
            'position'    : {
                'zone'    : 'custom'
            },
            X : 200,
            Y : 160,
            Width : 400,
            'buttons'    : {
                'Закрыть'    : {
                    'action': function() {}
                }
            }
        });
    }
};

var vm = new CustomStreamViewModel();

var MusicSelectControl = undefined;

$(function(){
    for(var i=1; i<= 3; ++i){
        SetupMusicSearchInput("#music-search-input" + i);
    }

    // Задаем редактору запросов высоту в 20пикселей:
    $('.select2-input').css('height', 40);

    $('#create-personal-station').click(function(){
        var settings = {artists:[]};

        for(var i = 1; i<=3; i++){
            var artist = $("#music-search-input" + i).select2("data");

            if(artist == null)
                continue;

            settings.artists.push(artist.mbid);
        }

        if(settings.artists.length == 0){
            vm.utils().show_dialog_message("Ошибка", "Для создания станции требуется хотябы один исполнитель.");
            return;
        }

        settings.is_mixrecomendation = $('#isMixRecomendation:checked').val() == 'on';
        vm.create_stream_by_mbid(settings);
    });
});


function SetupMusicSearchInput(id) {
    //Настраиваем редактор запросов: music-search-input
    MusicSelectControl = $(id).select2({
        placeholder: "Поиск",
        minimumInputLength: 3,
        ajax: {
            url: "/api/radio/search_artist_by_name",
            type: "GET",
            contentType:'json',
            data: function (term, page) {
                return {
                    query: term
                };
            },
            results: function (data, page) {
                var r = vm.utils().get_response_as_object(data);

                return {results: r };
            }
        },
        formatResult: searchFormatResult,
        formatSelection: searchFormatSelection
    });
}


function searchFormatResult(item) {
    var markup = "<div><img width='24px' src='/static/images/singer.png'/><span style='margin-left:5px; text-align: center'>" + item.artist_name + " (" + item.tracks_count + " трэков)" + "</span></div>";

    return markup;
}

function searchFormatSelection(item) {
    return item.artist_name;
}
