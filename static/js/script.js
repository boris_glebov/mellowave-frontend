/*!
 * Socialplay v.1.0.0
 * Copyright 2012 wpnukes.com
 * 
 */

$(document).ready( function(){
	
	/*** Dialog Script ***/
	$("#dialog-login, #dialog-search").dialog({
		autoOpen: false,
		draggable: false,
		show: 'clip',
		hide: 'clip'
	});
		
		function dialogLogin_size (){

			var resolution = $(window).width();
			if(resolution >= 1200){
				$("#dialog-login").dialog( "option", "width", 1070 );
			}
			if(resolution <= 1199){
				$("#dialog-login, #dialog-search").dialog( "option", "width", 530 );
			}
			if(resolution <= 767){
				$("#dialog-login, #dialog-search").dialog( "option", "width", 400 );
			}
			if(resolution <= 480){
				var winW = $(window).width();
				$("#dialog-login, #dialog-search").dialog( "option", "width", winW );
			}

			
		} // dialogLogin_size function ends
		

		
		/*** Trigger Dialog ***/
		$('.user-login').on('click', this, function(e){
			$('.dialog-overlay').fadeIn();
			$( "#dialog-login" ).dialog('open');
			e.preventDefault();
		});
		$('.user-search').on('click', this, function(e){
			$('.dialog-overlay').fadeIn();
			$( "#dialog-search" ).dialog('open');
			e.preventDefault();
		});
		
		$('.ui-dialog .ui-dialog-titlebar-close').click( function(){
			$('.dialog-overlay').fadeOut();
		});
				
		/*** hide elements on ESCAPE ***/
    $(document).bind('keydown', function(e) {
		  if (e.keyCode == 27) {
			  $(".dialog-overlay").fadeOut();
		  }
    });


		/*** Alert script ***/
		$('.alert a.close').click( function(e){
			$(this).parents('.alert').fadeOut();
			e.preventDefault();
		});

		/*** tooltip ***/
		$(function() {
			$('.tooltip').tooltip({
				track: false,
				position: {
					my: "center bottom",
					at: "center top",
				}
			});
		});
		

		/*** News Ticker 
		////////////////////// ***/
		function tweet(){
			$('#tweet li:first').fadeOut( function () { $(this).appendTo($('#tweet')).fadeIn(); });
			var $li = $(this);
			//$li.addClass('current');
			$(this).addClass('current');
		}
		setInterval(function(){ tweet () }, 5000);


		/*** Collapsible
		//////////////////////////// ***/
		$('.collapsible .btn-expand').click( function(e){
				var collapse = $(this).parents('.collapsible:first').find('.collapse-content');
				$(collapse, this).slideToggle();
				$(this).toggleClass('btn-collapse');
				e.preventDefault();
		});

		/*** events location map
		//////////////////////////// ***/
		$('.event-details .event-desc .location').on('click', this, function(e){
				var boxHeight = $('ul.events li .event-box').height();
				var collapse = $(this).parents('li:first').find('.location-map');
				collapse.css('top',boxHeight);
				$(collapse, this).toggle();
				$(this).toggleClass('active');
				e.preventDefault();
		});
		$('.location-map .close').click( function(e){
				var locMap = $(this).parents('.location-map:first');
				var location = $(this).parents('li:first');
				$(locMap, this).hide();
				
				$('.event-details .event-desc .location', location).toggleClass('active');
				e.preventDefault();
		});
	
		/*** Menu in mobile versions ***/
		function mobileMenu(){
			$('.menu-bar').prepend('<div class="mobile-button">Menu</div>');
			$('.mobile-button').click (function(){
					$('.menu').slideToggle();
				});
		}
		mobileMenu();
		activeMenu();
		function activeMenu(){
			var resolution = $(window).width();
				var $li = $('.menu > li');
				if(resolution <= 767){
					$($li).each(function(){
						if($li.has('ul').length){
							if( $li.not('active')){
									$li.toggleClass('active');
								}
						} // if condition ends
					});
				}
				else if (resolution > 767){
					$('.menu li').removeClass('active');
				}
		} // activeMenu ends 
		
		dialogLogin_size(); // Dialog resizing function
		
		$(window).resize( function(){

			dialogLogin_size(); // Dialog resizing function

			var resolution = $(window).width();
			if(resolution > 767){
				$('.menu').show().removeAttr('style');
			} // if condition to match resolution ends

			activeMenu();

		});
		
		
	/*** convert tp-menu into select **/
	$("<select />").appendTo(".bottom .container .span9");
	
	$("<option />", {
		 "selected": "selected",
		 "value"   : "",
		 "text"    : "Categories"
	}).appendTo(".bottom .container .span9 select");
	
	$(".bottom .container .span9 ul.nav li a").each(function() {
	 var el = $(this);
	 $("<option />", {
			 "value"   : el.attr("href"),
			 "text"    : el.text()
	 }).appendTo(".bottom .container .span9 select");
	});
	
	$(".bottom .container .span9 select").change(function() {
		window.location = $(this).find("option:selected").val();
	}); /*** convert tp-menu ends ***/		
		
		
	/*** Home page slider ***/	
	if( $('#da-slider').length !== 0 ){
		$('#da-slider').cslider();
	}
	
	/*** jcarousel slider ***/	
	if( $('.picSlide').length !== 0 ){
		jQuery('.picSlide').jcarousel({
				scroll: 1,
	//			auto: 2,
				wrap: 'circular',
		});
	}
		
	/*** jplayer initiate ***/
	if( $('#jquery_jplayer_2').length !== 0 ){

			new jPlayerPlaylist({
				jPlayer: "#jquery_jplayer_2",
				cssSelectorAncestor: "#jp_container_2"
			}, [
				{
					title:"Cro Magnon Man",
					free:true,
					mp3:"http://www.jplayer.org/audio/mp3/TSP-01-Cro_magnon_man.mp3",
					oga:"http://www.jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg"
				},
				{
					title:"Your Face",
					mp3:"http://www.jplayer.org/audio/mp3/TSP-05-Your_face.mp3",
					oga:"http://www.jplayer.org/audio/ogg/TSP-05-Your_face.ogg"
				},
				{
					title:"Cyber Sonnet",
					mp3:"http://www.jplayer.org/audio/mp3/TSP-07-Cybersonnet.mp3",
					oga:"http://www.jplayer.org/audio/ogg/TSP-07-Cybersonnet.ogg"
				},
				{
					title:"Tempered Song",
					mp3:"http://www.jplayer.org/audio/mp3/Miaow-01-Tempered-song.mp3",
					oga:"http://www.jplayer.org/audio/ogg/Miaow-01-Tempered-song.ogg"
				},
				{
					title:"Hidden",
					mp3:"http://www.jplayer.org/audio/mp3/Miaow-02-Hidden.mp3",
					oga:"http://www.jplayer.org/audio/ogg/Miaow-02-Hidden.ogg"
				}
			], {
				swfPath: "js",
				supplied: "oga, mp3",
				wmode: "window",
				smoothPlayBar: true,
				keyEnabled: true
			});
		
	} //  jplayer main if condition ends ***/
	
	
	
	
}); // document ready function ends //