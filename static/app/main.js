/**
 * Created by warlock on 11/5/13.
 */

define({
    app_name: 'TropsyApp',
    paths: {
        angular: 'vendor/angular.min',
        controllers: 'scripts/controllers'
    }
});

require([
    'app',
    'scripts/controllers/mainControllers'
], function(app, mainController){

});