# -*- coding: utf-8 -*-
__author__ = 'Boris'

from flask import Response, request, session, render_template
from functools import wraps
from utils.web_tools import get_user_id
from logger import logger
from datetime import datetime
from utils import vk

import json
import model

def _update_last_visit():
    user = model.db.UserEntity.get_first(get_user_id())

    if user is None:
        return

    try:
        user['last_visit'] = datetime.now()

        user.save()
    except Exception as ex:
        logger.exception(u"_update_last_visit error. {0}".format(ex.message), ex)


def _auth_user_vk():
    """
    Метод для аутентификации в ВК
    """
    if session.get('user') is not None and session['user']['is_auth']:
        _update_last_visit()
        return True

    session['user'] = {
        'user_id': 'None',
        'user_name': '',
        'is_auth': False,
        'access_token': '',
        'lastfm_session_key': None,
        'remote_addr': request.remote_addr
    }

    if not 'viewer_id' in request.args or not 'auth_key' in request.args:
        return False

    # Осуществляет проверку на подмену user_id
    if not vk.auth(request.args['viewer_id'], request.args['auth_key']):
        logger.fatal('Зафиксирована попытка взлома {0}'.format(request.args['viewer_id']))
        return False

    # Предварительная аутентификация пройдена
    session['user']['user_id'] = request.args.get('viewer_id')
    session['user']['is_auth'] = True
    session['user']['access_token'] = request.args.get('access_token')

    # Получим профиль пользователя из вк:
    profile = vk.get_profile(session['user']['user_id'])

    session['user']['first_name'] = profile['first_name']
    session['user']['last_name'] = profile['last_name']
    session['user']['photo'] = profile['photo_100']
    session['user']['sex'] = profile['sex']

    user = model.db.UserEntity.get_first(session['user']['user_id'])

    if user is None:
        user = model.db.UserEntity()
        user['user_id'] = session['user']['user_id']
        user['last_visit'] = datetime.now()

        if profile is not None:
            user['first_name'] = profile['first_name']
            user['last_name'] = profile['last_name']
            user['sex'] = profile['sex']
            user['avatar'] = profile['photo_100']

        try:
            user.validate()
            user.save()

            session['user']['is_auth'] = True
        except Exception as ex:
            session['user']['is_auth'] = False
            logger.exception(ex.message)

            #Перенаправляем на Internal Server Error
    else:
        user['last_visit'] = datetime.now()

        # Сохраняем ключ для API last.fm
        if 'lastfm_session' in user:
            session['user']['lastfm_session_key'] = user['lastfm_session']['session']['key']

        # Добавим недостающие поля.
        if profile is not None:
            user['first_name'] = profile['first_name']
            user['last_name'] = profile['last_name']
            user['sex'] = profile['sex']
            user['avatar'] = profile['photo_100']

        try:
            user.validate()
            user.save()
        except Exception as ex:
            logger.exception(ex.message)

    return True


def authenticate():
    """Sends a 401 response that enables basic auth"""
    return json.dumps({'access': False})


def oauth(f):
    @wraps(f)
    def decorated(*args, **kwargs):

        if _auth_user_vk():
            return f(*args, **kwargs)
        else:
            return render_template('page_access_denided.html')

    return decorated


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        user = session.get('user')

        if user is not None and not user['is_auth']:
            return authenticate()

        if user in 'remote_addr' and user['remote_addr'] != request.remote_addr:
            return authenticate()

        user['remote_addr'] = request.remote_addr

        return f(*args, **kwargs)
    return decorated