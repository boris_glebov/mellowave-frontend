# -*- coding: utf-8 -*-
__author__ = 'Boris'

from abc import ABCMeta
from flask import render_template, session, current_app


class BaseController:
    __metaclass__ = ABCMeta

    def page_not_found(self):
        return render_template("page_not_found.html"), 404

    def __init__(self):
        self.session = session
        self.name = "Controller"

    @property
    def app(self):
        return current_app

    @property
    def cache(self):
        if current_app is None:
            raise Exception("App not initalize")

        return current_app.cache