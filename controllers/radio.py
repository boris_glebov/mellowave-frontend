# -*- coding: utf-8 -*-
__author__ = 'Boris'

import utils.cache_manager as cache_manager
import errors
import constants

from flask import render_template, session
from controller import BaseController
from model import db
from asq.initiators import *
from random import shuffle
from logger import logger


class RadioController(BaseController):

    def radio(self):
        """
        Содержит логику главной страницы раздела "Радио".
        Подготовляет группы, рекомендованные станции и раздел избранное.
        """

        user_from_session = self._get_session_user()

        if user_from_session is None:
            favorite_stations = []
            personal_stations = []
        else:
            all_favorite_stations = user_from_session.get('favorite_stations')

            if all_favorite_stations is None:
                personal_stations = []
            else: 

                personal_stations = query(all_favorite_stations).where(
                    lambda fs: 'station_type' in fs and
                               (fs['station_type'] == constants.RADIO_STATION_TYPE.PERSONAL
                               or fs['station_type'] == constants.RADIO_STATION_TYPE.GENRE)).to_list()

        return render_template("radio.html",
                               personal_stations=personal_stations,
                               user=self._get_session_user())

    def radio_group(self, group_name):
        """
        Логика страницы группы. Содержит список радио станций входящих в группу.
        """

        # Ищем группу по ее имени:
        group = db.RadioStationGroupEntity.find_one({'group_id': group_name})

        if group is None:
            return self.page_not_found()

        # Получаем список станций которые относятся к данной группе:

        stations = list(db.RadioStationEntity.find({'group_id': group_name}))

        return render_template("radio_group.html",
                               group=group,
                               stations=stations,
                               user=self._get_session_user())

    def radio_station(self, station_id):
        """
        Логика страницы радио станции.
        """

        user_from_session = self._get_session_user()

        favorite_stations = []

        if user_from_session is None:
            is_station_favorite = False
        else:
            favorite_stations = user_from_session.get('favorite_stations', [])

            is_station_favorite = query(favorite_stations if favorite_stations is not None else []) \
                .any(lambda x: x['station_id'] == station_id)

        # Ищем станцию по ее имени:
        station = db.RadioStationEntity.find_one({"station_id": station_id})

        if station is None:
            return self.page_not_found()

        return render_template('radio_station.html',
                               station=station,
                               random_stations=list(query(self._generate_random_stations(favorite_stations)).take(2)),
                               is_station_favorite=is_station_favorite,
                               user=self._get_session_user())

    def radio_by_artist(self, artist_id):
        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        station = cache_manager.get_person_station(user['user_id'], artist_id, self.cache)

        if station is None:
            return errors.station_not_found(artist_id)

        return render_template('radio_station.html',
                               station=station,
                               is_station_favorite=False,
                               user=user
                               )

    def radio_by_station_id(self, station_id):
        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        station = cache_manager.get_person_station(user['user_id'], station_id, self.cache)

        if station is None:
            return errors.station_not_found(station_id)

        return render_template('radio_station.html',
                               station=station,
                               is_station_favorite=station['is_favorite'] if 'is_favorite' in station else False,
                               user=user)

    def prepare_personal_station(self):
        return render_template('prepare_personal_station.html', user=self._get_session_user())

    def prepare_genre_station(self):
        return render_template('prepare_genre_station.html', user=self._get_session_user())

    #
    # Private Methods
    #

    def _get_session_user(self):
        if session.get('user') is not None and session['user']['is_auth']:
            user = db.UserEntity.find_one({'user_id': session['user']['user_id']})

            if user is not None:
                return user
            else:
                return None
        else:
            return None

    def _generate_random_stations(self, favorite_stations):
        """
        Метод формирует список случайных станция для рекомендации пользователю.
        В список случайных станций не должны входить станции, которые уже добавлены в закладки пользователя
        """

        find_stations = []

        for station in db.RadioStationEntity.find():
            if favorite_stations is not None and len([fs for fs in favorite_stations
                                                      if fs['station_id'] == station['station_id']]) > 0:
                continue
            else:
                find_stations.append(station)

        shuffle(find_stations)

        random_station_count = 2 if len(favorite_stations) > 0 else 4

        shuffle(find_stations)

        if len(find_stations) < random_station_count:
            random_stations = find_stations
        else:
            random_stations = []
            for n in range(0, random_station_count):
                random_stations.append(find_stations[n])

        return random_stations
