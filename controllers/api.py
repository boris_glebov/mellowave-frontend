# -*- coding: utf-8 -*-


__author__ = 'Boris'


from controller import BaseController
from model import db
from asq.initiators import *
from random import shuffle, randint
from logger import logger
from mongokit import ObjectId

import datetime
import errors
import utils.lastfm_proxy as lastfm
import utils.statistic as statistic
import utils.session_manager as session_manager
import utils.cache_manager as cache_manager
import constants
import utils.echonest as echonest
import json


class ApiContoller(BaseController):

    #
    # Public Methods
    #

    def get_track_vk_id(self, station_id, track_id):
        """
        Возвращает id записи включающий в себя id трэка + id владельца
        """

        station = db.RadioStationEntity.find_one({"station_id": station_id})

        if station is None:
            logger.error("GetTrackVkId. Station '{0}' not found.".format(station_id))
            return {
                'code': 404,
                'error': "Station not found."
            }

        track = query(station.get_safe("tracks", [])).where(lambda x: x["hash"] == track_id).first_or_default(None)

        if track is None:
            return {
                'code': 404,
                'error': "Track not found."
            }

        vkid = track["vkid"].split('_')
        return {
            'owner_id': vkid[0],
            'track_id': vkid[1]
        }

    def get_current_song(self, mount):
        """
        Возвращает название играющего сейчас трэка на канале

        Формат трэка в кэше:

        <Исполнитель> - <Трэк>|<Hash>
        """

        # Обновим слушателей:
        self._update_listener_list(mount)

        node = cache_manager.get_history_tracks(mount, self.cache)

        if node is None or len(node) == 0:
            return ''

        split = node[-1].split('|')
        track = {
            "track_id": split[-1],
            "track_name": split[0]
        }

        return track

    def get_history_songs(self, mount):
        """
        Получить список последних трэков, прозвучавших на канале.
        """

        node = cache_manager.get_history_tracks(mount, self.cache)

        if node is None or len(node) == 0:
            return []

        return [song.split('|')[0] for song in node[::-1]][::-1][:-1]

    def get_listeners(self, mount):
        """
        Получить список слушателей на канале.
        """

        listeners = cache_manager.get_listeners(mount, self.cache)

        return listeners if listeners is not None else []

    def get_station_stat(self, mount, now_playing=''):
        """
        Вовзращает общую статистику по станции:

        - Кол-во слушателей
        - Текущий трэк
        """

        # Получаем кол-во слушателей:
        listeners = cache_manager.get_listeners(mount, self.cache)

        if listeners is not None:
            listener_count = len(listeners)
        else:
            listener_count = 1

        # Формируем объект ответа:

        song = self.get_current_song(mount)

        if song is None or song == '':
            return {
                'code': 404,
                'error': 'song not found.'
            }

        #Пробуем извлечь из трэка исполнителя:
        song_split = [s.strip() for s in song["track_name"].split('-')]
        artist = song_split[0] if len(song_split) >= 2 else song

        # Если начался новый трэк, то записываем предыдущий в прослушанные композиции:
        #prev_track = self.session.get('prev_track')

        if  now_playing == song['track_id']:
            return {
                'current_song': song,
                'listener_count': listener_count,
                'is_short': True,
            }

        # Устанавливаем новый трек в качестве предыдущего:
        prev_track = {
            'track_id': song['track_id'],
            'track_name': song['track_name'],
            'track_short_name': song_split[1],
            'artist_name': artist
        }

        self.session['prev_track'] = prev_track

        # Скроблим трэк
        self._scrobble(mount, prev_track["track_id"], song_split[0], song_split[1])

        artist_info = self.get_artist_info(artist)

        stat = {
            'current_song': song,
            'listener_count': listener_count,
            'artist': artist,
            'bio': self.get_bio_from_artist_info(artist_info),
            'tags': self.get_tags_from_artist_info(artist_info),
            'slides': self._get_slides_from_track(mount, song['track_id']),
            'track_info': self._get_track_info(mount, song['track_id']),
            'is_short': False
        }

        return stat

    def add_favorite_station(self, station_id, station_type):
        """
        Добавляет радио-станцию в избранное пользователя
        """

        user_id = self.session['user']['user_id']
        user = db.UserEntity.find_one({'user_id': user_id})

        if user is None:
            return ""

        if station_type == constants.RADIO_STATION_TYPE.EXPERT:
            station = db.RadioStationEntity.find_one({'station_id': station_id})
        else:
            station = cache_manager.get_person_station(user_id, station_id, self.cache)

        if station is None:
            return errors.station_not_found(station_id)

        if 'favorite_stations' not in user:
            user['favorite_stations'] = []

        fav_station = query(user['favorite_stations']).where(lambda x: x['station_id'] == station_id).first_or_default(None)

        if fav_station is not None:
            #Удаляем станцию
            user['favorite_stations'].remove(fav_station)

            statistic.favorite_station_stat(user_id, station_id, False)

            response = {
                'response_type': 'unfavorite',
                'status': True
            }
        else:
            user['favorite_stations'].append({
                'station_id': station['station_id'],
                'name': station['name'],
                'tiles': station['tiles'],
                'station_type': station_type,
                'artists': station['artists_id'] if 'artists_id' in station else []
            })

            statistic.favorite_station_stat(user_id, station_id, True)

            response = {
                'response_type': 'favorite',
                'status': True
            }

        try:
            user.validate()
            user.save()
        except Exception as ex:
            logger.error(u'Method add_favorite_station. Station: {0}; User: {1}.\n{2}'.format(station_id,
                                                                                              user['user_id'],
                                                                                              ex))
            return errors.internal_server_error()

        return response

    def add_favorite_song(self, station_id, song_id, artist, title):
        """
        Like играющего трэка на станции
        """
        user = self.session['user']

        trackLike = db.TrackLikeEntity.find_one({'user_id': user['user_id'],
                                                 'track_id': song_id,
                                                 })

        trackBan = db.TrackBanEntity.find_one({'user_id': user['user_id'],
                                               'track_id': song_id,
                                               })
        split_title = title.split('-')

        if len(split_title) == 2:
            title = split_title[1].strip()

        try:
            if trackLike is None:
                trackLike = db.TrackLikeEntity()

                trackLike.user_id = user['user_id']
                trackLike.track_id = song_id
                trackLike.station_id = station_id
                trackLike.is_like = True
                trackLike.artist = artist
                trackLike.title = title

                if trackBan is not None and trackBan['is_ban']:
                    self._setAttr(trackBan, 'is_ban', False)
                    statistic.block_track_stat(user['user_id'], station_id, song_id, False)

                try:
                    lastfm_key = session_manager.get_lastfm_key(self.session)

                    #love in last.fm
                    if lastfm_key is not None:
                        lastfm.loveTrack(lastfm_key,
                                         title,
                                         artist)
                except Exception as ex:
                    logger.exception(ex)
            else:
                trackLike['is_like'] = not trackLike.is_like

                if trackBan is not None and trackBan['is_ban'] and trackLike['is_like']:
                    self._setAttr(trackBan, 'is_ban', False)
                    statistic.block_track_stat(user['user_id'], station_id, song_id, False)

                try:
                    lastfm_key = session_manager.get_lastfm_key(self.session)

                    #unlove in last.fm
                    if lastfm_key is not None:
                        lastfm.unloveTrack(lastfm_key,
                                           title,
                                           artist)
                except Exception as ex:
                    logger.exception(ex)

            trackLike.validate()
            trackLike.save()

            statistic.favorite_track_stat(user['user_id'], station_id, song_id, trackLike.is_like)

            return {
                'is_like': trackLike.is_like
            }

        except Exception as ex:
            logger.error(ex.message)

            return {
                'error': 500,
                'message': 'Internal Server Error'
            }

    def ban_song(self, station_id, song_id):
        """
        Ban\unban track.
        """

        user = self.session['user']

        trackBan = db.TrackBanEntity.find_one({'user_id': user['user_id'],
                                               'track_id': song_id,
                                               'station_id': station_id})

        trackLike = db.TrackLikeEntity.find_one({'user_id': user['user_id'],
                                                 'track_id': song_id,
                                                 'station_id': station_id})

        if trackBan is None:
            trackBan = db.TrackBanEntity()

            trackBan['user_id'] = user['user_id']
            trackBan['track_id'] = song_id
            trackBan['station_id'] = station_id
            trackBan['is_ban'] = True

            if trackLike is not None and trackLike['is_like']:
                self._setAttr(trackLike, 'is_like', False)
                statistic.favorite_track_stat(user['user_id'], station_id, song_id, False)
        else:
            trackBan['is_ban'] = not trackBan['is_ban']

            if trackBan['is_ban'] and trackLike is not None and trackLike['is_like']:
                self._setAttr(trackLike, 'is_like', False)
                statistic.favorite_track_stat(user['user_id'], station_id, song_id, False)

        trackBan.validate()
        trackBan.save()

        statistic.block_track_stat(user['user_id'], station_id, song_id, trackBan['is_ban'])

        return {
            'is_ban': trackBan['is_ban']
        }

    def rename_personal_station(self, station_id, station_name):
        """
        Задать имя персональной станции.
        """

        MAX_NAME_LENGTH = 15

        user = self._get_session_user()

        if user is None:
            return errors.access_denied()

        station = cache_manager.get_person_station(user['user_id'], station_id, self.cache)

        if station is None:
            return errors.station_not_found(station_id)

        station['name']['eng'] = station_name[:MAX_NAME_LENGTH]
        station['name']['short'] = station_name[:MAX_NAME_LENGTH]

        cache_manager.set_person_station(user['user_id'], station_id, station, self.cache)

        # Если станция добавлена в избранные, то нужно изменить ее имя еще и БД
        if 'is_favorite' in station and station['is_favorite']:
            fav_station = query(user['favorite_stations']).where(
                lambda x: x['station_id'] == station_id).first_or_default(None)

            if fav_station is not None:
                fav_station['name']['eng'] = station_name
                fav_station['name']['short'] = station_name[:MAX_NAME_LENGTH]

                try:
                    user.save()
                except Exception as ex:
                    logger.exception('rename_personal_station error. {0}'.format(ex.message), ex)
        return {
            "status": "ok"
        }

    #
    # Функции по работе с радио исполнителя
    #

    def create_person_station_by_name(self, settings):
        """
        Функция создает пользовательское радио, эфир которого состоит только из выбранных групп.
        """

        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        # Получаем информацию об исполнителях из базы:
        artists = []
        for artist in settings['artists']:
            for find_artist in self.search_artist_by_name(artist.lower()):
                result = db.ArtistEntity.find_one({'mbid': find_artist['mbid']})

                if result is None:
                    continue

                artists.append(result)

        self.check_and_log_is_personal_station_empty(artists, settings)

        station_id = str(ObjectId()) if not 'station_id' in settings else settings['station_id']
        is_favorite = settings['is_favorite'] if 'is_favorite' in settings else False

        station_name = settings['station_name'] if 'station_name' in settings else ''
        short_name = settings['short_name'] if 'short_name' in settings else ''

        return self.create_person_station(artists, station_id, station_name, short_name, False, is_favorite)

    def create_person_station_by_mbid(self, settings):
        """
          Функция создает пользовательское радио, эфир которого состоит только из выбранных групп.
        """

        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        artists = list(db.ArtistEntity.find({"mbid": {"$in": settings['artists']}}))

        self.check_and_log_is_personal_station_empty(artists, settings)

        station_id = str(ObjectId()) if not 'station_id' in settings else settings['station_id']
        is_favorite = settings['is_favorite'] if 'is_favorite' in settings else False

        station_name = settings['station_name'] if 'station_name' in settings else ''
        short_name = settings['short_name'] if 'short_name' in settings else ''

        return self.create_person_station(artists, station_id, station_name, short_name,
                                          settings['is_mixrecomendation'],
                                          is_favorite)

    def create_person_station_by_genre(self, settings):
        """
        Формируем персональную станцию по жанрам
        """

        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        if 'genres' in settings:
            genres = settings['genres']
        elif 'artists' in settings:
            genres = settings['artists']
        else:
            genres = []

        station_id = str(ObjectId()) if not 'station_id' in settings else settings['station_id']
        is_favorite = settings['is_favorite'] if 'is_favorite' in settings else False

        if 'station_name' in settings:
            station_name = settings['station_name']
        elif len(genres) == 1:
            station_name = genres[0]
        else:
            station_name = u'Радио по жанрам'

        station = {
            'name': {
                'short': station_name,
                'eng': station_name},
            'station_id': station_id,
            'station_type': constants.RADIO_STATION_TYPE.GENRE,
            'tracks': echonest.get_playlist_by_genres(genres),
            'tiles': [],
            'artists': genres,
            'artists_id': genres,
            'is_favorite': is_favorite
        }

        cache_manager.set_person_station(user['user_id'], station['station_id'], station, self.cache)

        return {
            'station_id': station['station_id']
        }

    def create_person_station_by_favorite(self):
        """
        Создаем станцию, состояющую только из избранных трэков
        """

        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        station_id = str(ObjectId())
        station_name = u'Избранное'

        tracks = []

        for track_like in db.TrackLikeEntity.find({'user_id': user['user_id'], 'is_like': True}):
            tracks.append({
                'name': track_like['title'],
                'artist': track_like['artist'],
                'mbid': track_like['track_id'],
                'artists': [track_like['artist']]
            })

        station = {
            'name': {
                'short': station_name,
                'eng': station_name},
            'station_id': station_id,
            'station_type': constants.RADIO_STATION_TYPE.FAVORITE,
            'tracks': tracks,
            'tiles': [],
            'artists': [],
            'artists_id': [],
        }

        if len(station['tracks']) >= 10:
            cache_manager.set_person_station(user['user_id'], station['station_id'], station, self.cache)

            return {
                'station_id': station['station_id']
            }
        else:
            return {
                'code': 1,
                'error': 'Для составления радио у тебя не достаточное кол-во избранных трэков.'
            }

    def create_person_station(self, artists, station_id, station_name, short_name, is_mixrecomendation, is_favorite):
        """
        Общий метод создания персональной станции.
        Станцию можно создавать либо по спискок имен исполнителей, либо по списку mbid.
        """

        user = self._get_session_user()

        # Формируем название станции
        if short_name == '':
            short_name = self._generate_personal_station_short_name(artists)
        if station_name == '':
            station_name = self._generate_personal_station_name(artists)

        # Создаем заготовку станции:
        station = {
            'name': {
                'eng': station_name,
                'short': short_name},
            'station_id': station_id,
            'station_type': constants.RADIO_STATION_TYPE.PERSONAL,
            'tracks': [],
            'tiles': [],
            'artists': [],
            'artists_id': [],
            'is_mixrecomendation': is_mixrecomendation,
            'is_favorite': is_favorite
        }

        for artist in artists:
            tracks = artist.get_safe('tracks', [])

            self._set_artist_name_for_track(artist['name'], tracks)

            # Добавим рекомендованные треки
            if is_mixrecomendation:
                tracks += echonest.get_playlist_by_artists([artist])

            station['tracks'] += tracks
            station['artists'] += [artist['name']]
            station['artists_id'] += [artist['mbid']]

        if len(station['tracks']) == 0:
            for artist in artists:
                statistic.person_station_empty_stat(artist['mbid'], artist['name'])

            return {
                'code': 1,
                'error': 'Недостаточно материала для формирования станции.'
            }

        cache_manager.set_person_station(user['user_id'], station['station_id'], station, self.cache)

        return {
            'station_id': station['station_id']
        }

    def create_favorite_personal_station(self, station_id):
        """
        Запускает сохраненую ранее персональную радио станцию
        """

        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        user_db = db.UserEntity.find_one({'user_id': self.session['user']['user_id']})

        fav_station = query(user_db.get_safe('favorite_stations', []))\
            .where(lambda st: st['station_id'] == station_id)\
            .first_or_default(None)

        if fav_station is None:
            return errors.station_not_found(station_id)

        settings = {
            'artists': fav_station['artists'] if 'artists' in fav_station else [],
            'station_id': station_id,
            'station_name': fav_station['name']['eng'],
            'short_name': fav_station['name']['short'],
            'is_mixrecomendation': 'is_mixrecomendation' in fav_station and fav_station['is_mixrecomendation'],
            'is_favorite': True
        }

        if fav_station['station_type'] == constants.RADIO_STATION_TYPE.GENRE:
            return self.create_person_station_by_genre(settings)
        else:
            return self.create_person_station_by_mbid(settings)

    def get_next_track(self, station_id):
        """
        Запустить следующий трэк

        Ошибка:

            1 - Список воспроизведения пуст
            2 - Вам не понравился ни один трэк на этой станции
            404 - Станция не найдена

        """

        user = self._get_session_user()

        if user is None:
            return errors.permission_denided()

        station = cache_manager.get_person_station(user['user_id'], station_id, self.cache)

        if station is None:
            return {
                'code': 404,
                'error': 'Станция не найдена.'
            }

        track_number = cache_manager.get_person_station_track_number(user['user_id'], station_id, self.cache)

        if track_number is None or 'rotate' not in station or len(station['rotate']) <= track_number + 1:
            track_number = 0

            # Новая ротация.
            self._rotate_artist_playlist(station)
        else:
            track_number += 1

        if len(station['rotate']) == 0:
            return {
                'code': 1,
                'error': 'Список воспроизведения пуст.'
            }

        # Проверим не забанен ли трэк

        track = None
        while True:
            if len(station['rotate']) < track_number:
                return {
                    'code': 2,
                    'error': 'Вам не понравился ни один трэк на этой станции'
                }

            track = station['rotate'][track_number]

            if not self._is_ban(track):
                break

            track_number += 1

        track_number += 1

        # Запоминаем текущий номер трэка
        cache_manager.set_person_station_track_number(user['user_id'], station_id, track_number, self.cache)

        cache_manager.set_person_station(user['user_id'], station_id, station, self.cache)

        # Скроблим трэк
        self._scrobble(station_id, track["mbid"], track['artist'], track['name'])

        # Получаем биографию артиста
        artist_info = self.get_artist_info(track['artist'])

        bio = self.get_bio_from_artist_info(artist_info)
        tags = self.get_tags_from_artist_info(artist_info)

        return {
            'title': track['name'],
            'artist': track['artist'],
            'mbid': track['mbid'],
            'bio': bio,
            'tags': tags,
            'track_info': self._get_track_info(station_id, track['mbid'])
        }

    def search_artist_by_name(self, q):
        """
        Поиск исполнителя по имени
        """

        lower_query = u'"{0}"'.format(q.lower().strip())

        search_result = db.radio_db.command("text", 'Artists', search=lower_query, project={'tracks': 0}, limit=100)

        artists = [{
                       "id": r["obj"]["mbid"],
                       "artist_name": r["obj"]["name"],
                       "tracks_count": r["obj"]["track_count"],
                       "mbid": r["obj"]["mbid"]
                   } for r in search_result['results'] if r["obj"]["track_count"] >= 10]

        artists.sort(key=lambda x: x['tracks_count'])

        return artists[::-1][:10]

    def search_genre_by_name(self, q):
        """
        Поиск жанра по имени
        """

        lower_query = u'"{0}"'.format(q.lower().strip())

        search_result = db.radio_db.command("text", 'Genres', search=lower_query, limit=10)

        genres = [{
            "id": str(r["obj"]["_id"]),
            "name": r["obj"]["name"]
        } for r in search_result['results']]

        return genres

    def get_similar_artists(self, artist_name):
        """
        Возвращает список связанных исполнителей
        """

        last_response = lastfm.getSimilarsArtsits(artist_name)

        if 'error' in last_response:
            return []

        if 'similarartists' in last_response and 'artist' in last_response['similarartists']:
            artists = last_response['similarartists']['artist']

            if isinstance(artists, list):
                shuffle(artists)
            else:
                return []

            similar_artists = []
            for artist in artists[:4]:
                photo = query(artist['image']).where(lambda x: x['size'] == 'extralarge').first_or_default('')

                similar_artists.append({
                    'name': artist['name'],
                    'photo': 'url({0})'.format(photo['#text'])
                })

            return similar_artists
        else:
            return []

    def i_m_here(self):
        """
        Отмечает пользователя онлайн
        """

        user = self._get_session_user()

        if user is None:
            return

        users = cache_manager.get_who_online(self.cache)

        if users is None:
            users = []

        you = query(users).where(lambda x: x['user_id'] == user['user_id']).first_or_default(None)

        if you is None:
            users.append({
                'user_id': user['user_id'],
                'user_name': u'{0} {1}'.format(user.get('first_name'), user.get('last_name')),
                'user_photo': user.get('photo'),
                'last_update': datetime.datetime.now()
            })
        else:
            you['last_update'] = datetime.datetime.now()

        # Удалим всех слушателей, которые не обновили uptime в течении 10 минут:
        lost_listeners = query(users) \
            .where(lambda x: (datetime.datetime.now() - x['last_update']).seconds >= 600) \
            .to_list()

        for l in lost_listeners:
            users.remove(l)

        # Перезаписываем пользователей
        cache_manager.set_who_online(users, self.cache)

    def who_online(self):
        """
        Возвращает список тех, кто сейчас на сайте
        """

        users = cache_manager.get_who_online(self.cache)

        if users is None:
            return json.dumps([])

        return users

    #
    # Private methods
    #

    def _update_listener_list(self, mount):
        """
        Обновляет список пользователей на канале.
        Так же сообщает, что вызвавший метод пользователь еще тут.
        """
        user = self.session['user']

        listeners = cache_manager.get_listeners(mount, self.cache)

        if listeners is None:
            listeners = []

        listener = query(listeners).where(lambda x: x['user_id'] == user['user_id']).first_or_default(None)

        # Создадим или обновим слушателя:
        if listener is None:

            if self.session['user']['is_auth']:
                user_info = {
                    'user_id': user['user_id'],
                    'user_name': u'{0} {1}'.format(user.get('first_name'), user.get('last_name')),
                    'user_photo': user.get('photo'),
                    'last_update': datetime.datetime.now(),
                    'listened_track_count': statistic.get_listened_track_count(user['user_id'])
                }

                listeners.append(user_info)
        else:
            listener['last_update'] = datetime.datetime.now()

        # Удалим всех слушателей, которые не обновили uptime в течении 10 минут:
        lost_listeners = query(listeners) \
            .where(lambda x: (datetime.datetime.now() - x['last_update']).seconds >= 600) \
            .to_list()

        for l in lost_listeners:
            listeners.remove(l)

        # Перезаписываем пользователей
        cache_manager.set_listenres(mount, listeners, self.cache)

    def _is_ban(self, track):
        """
        Проверим не забанен ли трэк
        """

        user = self._get_session_user()

        banned_track = db.TrackBanEntity.find_one({'user_id': user['user_id'], 'track_id': track['mbid']})

        return banned_track is not None

    def _get_track_info(self, station_id, track_id):
        """
        Получаем информацию по лайкам трэка
        """

        user = self.session['user']

        #Узнаем лайкну ли, пользователь этот трэк
        trackLike = db.TrackLikeEntity.find({'user_id': user['user_id'],
                                             'track_id': track_id,
                                             'is_like': True}).count()

        likes_count = db.TrackLikeEntity.find({
            'station_id': station_id,
            'track_id': track_id,
            'is_like': True
        }).count()

        trackBan = db.TrackBanEntity.find({'user_id': user['user_id'],
                                           'track_id': track_id,
                                           'is_ban': True}).count()

        return {
            'station_id': station_id,
            'track_id': track_id,
            'is_user_like': trackLike > 0,
            'is_user_ban': trackBan > 0,
            'likes': likes_count
        }

    def _get_slides(self, artist_info, station_id):
        """
        Возвращает коллекцию картинок по артисту
        """

        if artist_info is None or not 'image' in artist_info['artist']:
            return self._get_slides_from_station(station_id)

        mega_image = query(artist_info['artist']['image']).where(lambda x: x["size"] == 'mega').first_or_default(None)

        if mega_image is None or mega_image['#text'] == '':
            return self._get_slides_from_station(station_id)

        return [mega_image['#text']]

    def _get_slides_from_station(self, station_id):
        """
        Извлекает картинки для слайд шоу из сущности "Радио станция"
        """

        station = db.RadioStationEntity.find_one({'station_id': station_id})

        if station is None:
            return []

        return station['tiles']

    def _get_slides_from_track(self, station_id, track_id):
        """
        Извлекает картинки для слайд шоу из сущности "Трэк"

        (В дальнейшем картинки должны браться из сущности "Исполнитель")
        """

        # Очищаем сессию:
        self.session['current_song_images'] = []

        station = db.RadioStationEntity.find_one({'station_id': station_id})

        if station is None:
            return None

        track = query(station.get_safe('tracks', [])).where(lambda t: t['hash'] == track_id).first_or_default(None)

        if track is None:
            return None

        if 'artist_images' not in track or track['artist_images'] is None:

            return ''

        shuffle(track['artist_images'])

        self.session['current_song_images'] = track['artist_images']

        return track['artist_images']


    def get_artist_info(self, artist):
        """
        Получаем инофрмацию об исполнители с Ласт.фм
        """

        artist_info = None
        try:
            artist_info = lastfm.getArtistInfo(artist)
        except Exception as ex:
            logger.error("LastFM.getArtistInfo Error. {0}".format(ex.message))

        return artist_info


    def get_bio(self, artist):
        """
        По имени исполнителя пытается получить биографию с ласт.фм. Если получить не удалось,то выводит ссылку на
        редактирования исполнитлея в базе ласт.фм
        """

        artist_info = self.get_artist_info(artist)

        return self._execute_bio_from_artist_info(artist_info)

    def get_tags(self, artist):
        """
        Получаем тэги связанные с исполнителем с Ласт.фм
        """

        artist_info = self.get_artist_info(artist)

        return self._execute_tags_from_artist_info(artist_info)

    def get_bio_from_artist_info(self, artist_info):
        try:
            return self._execute_bio_from_artist_info(artist_info)
        except Exception as ex:
            logger.exception(u'get_bio_from_artist_info. {0}'.format(ex.message))

            return ''

    def get_tags_from_artist_info(self, artist_info):
        try:
            return self._execute_tags_from_artist_info(artist_info)
        except Exception as ex:
            logger.exception(u'get_tags_from_artist_info error. {0}'.format(ex.message))

            return []

    def _execute_bio_from_artist_info(self, artist_info):
        """
        Излвекаем биографию из информации о артисте полученной с Ласт.ФМ
        """

        if artist_info is None or artist_info["artist"]["bio"]["content"] == u'':
            bio = u'<div style="background: #eef5fc">Описание этого исполнителя пока отсутствует, <a href="http://www.lastfm.ru/music/{0}/+wiki/edit" target="_blank">хочешь помочь</a>?</div>' \
                .format(artist_info["artist"]["name"])
        else:
            bio = artist_info["artist"]["bio"]["content"]

        return bio

    def _execute_tags_from_artist_info(self, artist_info):
        tags = artist_info['artist']['tags']['tag']

        return tags

    def _scrobble(self, station_id, track_id, artist, title):
        """
        Отмечает трэк на ласт.фм, как прослушенный. Записывает лог.
        """

        user_id = self.session['user']['user_id']

        statistic.track_scrobble_stat(user_id, station_id, track_id, u'{0} - {1}'.format(artist, title))

        # Скроблим трэк на last.fm
        try:
            lastfm_key = session_manager.get_lastfm_key(self.session)
            if lastfm_key is not None:
                lastfm.scrobbleTrack(lastfm_key, title, artist)
        except Exception as ex:
            logger.exception(ex)

    def _set_artist_name_for_track(self, artist_name, tracks):
        for track in tracks:
            track['artist'] = u'{0}'.format(artist_name)

    def _setAttr(self, entity, attribute_name, value):
        """
        Устанавливает значения атрибуты сущности БД и сохраняет сущность.

        Сущность - документ из MongoDB
        """

        entity[attribute_name] = value

        entity.validate()
        entity.save()

    def _get_session_user(self):
        if self.session['user']['is_auth']:
            user = db.UserEntity.find_one({'user_id': self.session['user']['user_id']})

            if user is not None:
                return user
            else:
                return None
        else:
            return None

    def _get_session_user(self):
        if 'user' in self.session and 'is_auth' in self.session['user'] and self.session['user']['is_auth']:
            user = db.UserEntity.find_one({'user_id': self.session['user']['user_id']})

            if user is not None:
                return user
            else:
                return None
        else:
            return None

    def _rotate_artist_playlist(self, artist_station):
        """
        Формируем эфир персональной станции
        """

        artist_station['rotate'] = []
        sorted_tracks = sorted(artist_station['tracks'], key=lambda track: track['name'])
        tracks = self._distinct_track_collection(sorted_tracks)

        track_by_artist = query(tracks).group_by(lambda track: track['artist'], lambda track: track).to_list()

        if len(track_by_artist) == 1:
            l = list(track_by_artist[0])
            shuffle(l)

            artist_station['rotate'] = l

            return

        # Подготовим очереди:
        pools = []
        for group in track_by_artist:
            l = list(group)
            shuffle(l)
            pools.append(l)

        # Кол-во пулов с трэками:
        artist_count = len(pools)
        pool_idx = []

        while artist_count > 0:
            if artist_count == 1:
                artist_station['rotate'] += pools[0]
                break

            if len(pool_idx) == 0:
                pool_idx = range(0, artist_count)

            shuffle(pool_idx)

            for pool_id in range(0, artist_count):
                if len(pools[pool_idx[pool_id]]) > 0:
                    artist_station['rotate'].append(pools[pool_idx[pool_id]].pop())
                else:
                    artist_count -= 1

    def _distinct_track_collection(self, sorted_tracks):
        """
        Убираем дубликаты трэков
        """
        seen = set()

        tracks = []

        for t in sorted_tracks:
            if t['name'] not in seen:
                tracks.append(t)
                seen.add(t['name'])

        return tracks

    def _generate_personal_station_name(self, artists):
        return ", ".join(artist['name'] for artist in artists)

    def _generate_personal_station_short_name(self, artists):
        """
        Формируем название персональной станции из имен исполнителей.
        Имя станции не должно превышать 15 символов.
        """

        MAX_SIZE = 12

        station_name = u'{0}...'.format(self._generate_personal_station_name(artists)[:MAX_SIZE])

        return station_name


    def check_and_log_is_personal_station_empty(self, artists, settings):
        """
         Логируем именя исполнителей, которых не нашли в БД:
        """
        if len(artists) == 0:
            for artist in settings['artists']:
                statistic.person_station_empty_stat(artist, artist)
