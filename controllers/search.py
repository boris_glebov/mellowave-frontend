# -*- coding: utf-8 -*-
from asq.initiators import query

__author__ = 'warlock'

from controller import BaseController
from model import db
from asq.initiators import *

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from oauth2client.tools import argparser

DEVELOPER_KEY = "AIzaSyDPhkn3ceN2xaVM8xdLwZFCEZzh7-oJRrg"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

from flask import render_template, session


def get_track_id(query):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION, developerKey=DEVELOPER_KEY)

    search_response = youtube.search().list(
        q=query,
        part="id,snippet",
        maxResults=1
        ).execute()

    for search_result in search_response.get("items", []):
        if search_result["id"]["kind"] == "youtube#video":
            return search_result["id"]["videoId"]


class SearchController(BaseController):
    def search(self):
        print get_track_id(u"Ария - Ночь Короче Дня")

        return render_template('search.html')