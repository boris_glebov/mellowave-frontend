# -*- coding: utf-8 -*-
__author__ = 'Boris'

# Модуль содержит объекты ответов на ошибки


#
# Общий тип ошибок
#

def error(message='', code=-1):
    return {
        'code': code,
        'error': message,
        'response_type': 'error',
        'message': message
    }


def short_error(code, error):
    return {
        'code': code,
        'error': error
    }


def access_denied(message=''):
    """
    Доступ запрещен. Пользователь не прошел аутентификацию
    """
    return {
        'response_type': 'error',
        'access': False,
        'message': message
    }


def internal_server_error():
    return {
        'response_type': 'error',
        'message': 'Internal Server Error (500)'
    }

#
# Ошибки связанные с пользователями
#

def user_not_found():
    return error("Учетная запись пользователя не найдена")

#
# Ошибки связанные с радио станцией
#

def station_not_found(station_id):
    """
    Станция с id = station_id не найдена
    """

    return error("Радио станция '{0}' не найдена.".format(station_id), 404)

def permission_denided():
    return {
        'code': 550,
        'error': 'Пользователь не прошел аутентификацию.'
    }

def artist_station_undefined_error():
    return {
        'code': 1,
        'error': 'К сожалению у нас недостаточно материла, чтобы сформировать станцию :('
    }