# -*- coding: utf-8 -*-
from flask import Flask,  render_template, session

from controllers_setup import ControllerFactory
from logger import logger
from werkzeug.contrib.cache import MemcachedCache
from extensions.memcached_session import Session

import constants
import settings


app = Flask(__name__)

# Настраиваем memcached:
app.cache = MemcachedCache(['{0}:{1}'.format(settings.MEMCACHED_SERVER, settings.MEMCACHED_PORT)])

app.session_interface = Session()
app.secret_key = settings.SECRET_KEY

api_controller = ControllerFactory.Create(constants.API_CONTROLLER)

#
#Register module

from modules import \
    radio as radio_module, \
    main as main_module, \
    api as api_module, \
    statistics as statistics_module, \
    search as search_module

app.register_blueprint(main_module.blueprint)
app.register_blueprint(api_module.blueprint)
app.register_blueprint(radio_module.blueprint)
app.register_blueprint(statistics_module.blueprint)
app.register_blueprint(search_module.blueprint)

#
# Setup App


@app.after_request
def add_p3p_headers(response):
    """
    Добавляет к каждому запросу заголовок P3P. Данный заголовок разрешает IE сохранять куки от IFrame сайта.
    """
    response.headers['P3P'] = 'CP="NOI ADM DEV COM NAV OUR STP"'

    return response


@app.after_request
def i_m_online(response):
    """
    Пользователь сообщает, что он онлайн
    """
    api_controller.i_m_here()

    return response


#
# Error handles


@app.errorhandler(404)
def page_not_found(error):
    return render_template("page_not_found.html"), 404


if __name__ == '__main__':
    try:
        app.run(debug=True, port=8080)

    except Exception as ex:
        logger.exception('Unhandle exception. {0}'.format(ex.message))