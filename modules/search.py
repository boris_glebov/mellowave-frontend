# -*- coding: utf-8 -*-
__author__ = 'warlock'

import constants

from flask import Blueprint, render_template
from controllers_setup import ControllerFactory
from decorators.authenticate import requires_auth, oauth
from utils.web_tools import *

blueprint = Blueprint('search', __name__, template_folder='templates')
search_controller = ControllerFactory.Create(constants.SEARCH_CONTROLLER)


@oauth
@blueprint.route('/search')
def search():
    return search_controller.search()