# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

from flask import Blueprint, render_template, current_app, session, request
from controllers_setup import ControllerFactory
from bson import json_util

from decorators.authenticate import requires_auth
from logger import logger


import constants
import settings
import json


blueprint = Blueprint('statistics', __name__, template_folder='templates')

api_controller = ControllerFactory.Create(constants.API_CONTROLLER)


@blueprint.route('/api/statistic/who_online/')
def get_who_online():
    """
    Возвращает кол-во пользователей на сайте
    """
    return json.dumps(api_controller.who_online(), default=json_util.default)


