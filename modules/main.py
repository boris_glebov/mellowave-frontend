# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

from flask import Blueprint, request, redirect, render_template
from logger import logger
from utils.web_tools import *
from decorators.authenticate import oauth

import utils.lastfm_proxy as lastfm
import model


blueprint = Blueprint('main', __name__, template_folder='templates')


@blueprint.route('/')
#@oauth
def index():
    """
    Главная страница. Приложение рассчитана на запуск только из соц. сети Вконтакте.
    Далее происходит дополнительная аутентификация пользователя.
    """

    return redirect('/search')

@blueprint.route('/last_auth')
def last_auth():
    """
    Last.fm аутентификация. Для доступа к методам на запись.
    """

    token = request.args.get('token')

    if 'user' not in session:
        return redirect('http://www.lastfm.ru/home')

    user = model.db.UserEntity.find_one({'user_id': get_user_id()})

    if user is None:
        return redirect('http://www.lastfm.ru/home')

    try:

        user['lastfm_session'] = lastfm.getSession(token)

        # Обновляем сессию last.fm в кэше:
        session['user']['lastfm_session_key'] = user['lastfm_session']['session']['key']

        user.validate()
        user.save()

    except Exception as ex:
        logger.exception(u'Last.fm auth error. {0}'.format(ex.message), ex)

    return redirect('http://www.lastfm.ru/home')