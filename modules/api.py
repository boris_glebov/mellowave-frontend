# -*- coding: utf-8 -*-
__author__ = 'Boris'

from flask import Blueprint, render_template, current_app, session, request
from controllers_setup import ControllerFactory
from decorators.authenticate import requires_auth
from logger import logger


import constants
import settings
import json

blueprint = Blueprint('api', __name__, template_folder='templates')

api_controller = ControllerFactory.Create(constants.API_CONTROLLER)


@blueprint.route('/api/radio/get_current_song/<mount>')
def get_current_song(mount):
    """
    Возврщает название трэка играющего на станции. Название берется из icecast2.
    """

    try:
        return json.dumps(api_controller.get_current_song(mount))
    except Exception as ex:
        logger.exception(u'get_current_song. {0}'.format(ex.message))


@blueprint.route('/api/radio/get_track_vk_id/station/<station_id>/track/<track_id>')
def get_track_vk_id(station_id, track_id):
    """
    Для некоторых операций требуется получить id трэка из ВК.
    Мы сохраняем его в базе при формировании станции из админки.
    """
    return json.dumps(api_controller.get_track_vk_id(station_id, track_id))


@blueprint.route('/api/radio/get_history_songs/<mount>')
def get_history_songs(mount):
    """
    Последнии 10 трэков, проигранных на станции.
    """
    return json.dumps(api_controller.get_history_songs(mount))


@blueprint.route('/api/radio/get_listeners/<mount>')
def get_listeners(mount):
    """
    Список слушателей (только тех, кто сейчас в онлайн на станции).
    """
    return render_template('dialogs/listeners_dialog.html', listeners=api_controller.get_listeners(mount))


@blueprint.route('/api/radio/get_station_stat/<mount>/now_playing/<now_playing>')
def get_station_stat(mount, now_playing):
    """
    Агрегирующий метод.
    Возвращает текущего исполнителя, трэк, биографию, фото исполнителя и еще доп. статистику.
    """
    try:
        #logger.info('start get_station_stat')
        return json.dumps(api_controller.get_station_stat(mount, now_playing))
    except Exception as ex:
        logger.exception(u'get_station_stat exception. {0}'.format(ex.message), ex)


@requires_auth
@blueprint.route('/api/radio/create_person_station/by/name', methods=["POST"])
def create_person_station_by_name():
    """
    Создание собственного эфира.
    Эфир состоит только из выбранных исполнителей.
    POST - запросом приходит коллекция имен исполнителей.

    Формат:

    {
        "artists": ["Sonata Artica", "Hibria]
    }
    """
    try:
        if request.data is None or request.data == '':
            return {
                'code': 1,
                'error': "Пустой запрос.",
            }

        return json.dumps(api_controller.create_person_station_by_name(json.loads(request.data)))

    except Exception as ex:
        logger.exception(u'create_custom_stream error. {0}'.format(ex.message), ex)

        return json.dumps({
            "code": 500,
            "error": "Internal Server Error"
        })


@requires_auth
@blueprint.route('/api/radio/create_person_station/by/genre', methods=["POST"])
def create_person_station_by_genre():
    """
    Создание собственного эфира.
    Эфир состоит только из исполнителей выбранных жанров.

    POST - запросом приходит коллекция имен жанров.

    Формат:

    {
        "genres" : ["rock", "classic rock"]
    }
    """

    try:
        if request.data is None or request.data == '':
            return {
                'code': 1,
                'error': "Пустой запрос."}

        return json.dumps(api_controller.create_person_station_by_genre(json.loads(request.data)))

    except Exception as ex:
        logger.exception(u'create_custom_stream error. {0}'.format(ex.message), ex)

        return json.dumps({
            "code": 500,
            "error": "Internal Server Error"
        })


@requires_auth
@blueprint.route('/api/radio/create_person_station/by/mbid', methods=['GET', "POST"])
def create_person_station_by_mbid():
    """
    Создание собственного эфира.
    Эфир состоит только из выбранных исполнителей.
    POST - запросом приходит коллекция ID исполнителей.

    ID исполнителя - это MBID (MusicBrainz ID).

    Формат:

    {
        "artists": ["319b1175-ced9-438f-986b-9239c3edd92d", "bbd0fd67-84d8-4291-bc19-40d83f4998e0]
    }
    """
    try:
        if request.data is None or request.data == '':
            return {
                'code': 1,
                'error': "Пустой запрос."}

        response = api_controller.create_person_station_by_mbid(json.loads(request.data))
        return json.dumps(response)

    except Exception as ex:
        logger.exception(u'create_custom_stream error. {0}'.format(ex.message), ex)

        return json.dumps({
            "code": 500,
            "error": "Internal Server Error"
        })


@requires_auth
@blueprint.route('/api/radio/create_person_station/by/favorite', methods=['GET', "POST"])
def create_person_station_by_favorite():
    """

    """

    try:
        response = api_controller.create_person_station_by_favorite()
        return json.dumps(response)
    except Exception as ex:
        logger.exception(u'create_person_station_by_favorite error. {0}'.format(ex.message), ex)


@requires_auth
@blueprint.route('/api/radio/create_fav_person_station/<station_id>')
def create_fav_person_station(station_id):
    """
    Подготавливает к прослушиванию персональное радио, которое ранее было добавлено в избранное.
    """
    try:
        return json.dumps(api_controller.create_favorite_personal_station(station_id))
    except Exception as ex:
        logger.exception(u'create_fav_person_station error. {0}'.format(ex.message), ex)


@requires_auth
@blueprint.route('/api/radio/get_next_track/<artist_id>')
def get_next_track(artist_id):
    """
    Метод возвращает следущий по порядку трэк.
    Используется для персонального радио.
    """
    return json.dumps(api_controller.get_next_track(artist_id))


@blueprint.route('/api/radio/get_bio/<artist>')
def get_bio(artist):
    """
    Возвращает биографию исполнителя.

    artist - имя исполнителя.
    """
    return json.dumps(api_controller.get_bio(artist))


@requires_auth
@blueprint.route('/api/radio/add_favorite_station/<station_id>/station_type/<station_type>')
def add_favorite_station(station_id, station_type):
    """
    Пользователю понравилась станции. Добавлена в избранное.
    Избранные станции отображаются на главной страницы в отдельном окне.
    """
    try:
        return json.dumps(api_controller.add_favorite_station(station_id, station_type))
    except Exception as ex:
        logger.exception(u'add_favorite_station exception. {0}'.format(ex.message), ex)


@requires_auth
@blueprint.route('/api/radio/add_favorite_song/station/<station_id>/song/<song>/artist/<artist>/title/<title>')
def add_favorite_song(station_id, song, artist, title):
    """
    Пользователю понравилась песня (в рамках станции).
    """
    try:
        return json.dumps(api_controller.add_favorite_song(station_id, song, artist, title))
    except Exception as ex:
        logger.exception(u'add_favorite_song exception. {0}'.format(ex.message), ex)


@requires_auth
@blueprint.route('/api/radio/ban_song/station/<station_id>/song/<song>')
def ban_song(station_id, song):
    """
    Добавить песню в исключения. Пользователь не хочет более слушать этот трэк.
    """
    try:
        return json.dumps(api_controller.ban_song(station_id, song))
    except Exception as ex:
        logger.exception(u'ban_song exception. {0}'.format(ex.message), ex)

        return {
            'error': 500,
            'message': 'Internal Server Error'
        }


@blueprint.route('/api/radio/search_artist_by_name', methods=['GET', 'POST'])
def search_artist_by_name():
    """
    Ищем исполнителей по имени.
    """
    if "query" not in request.args:
        return json.dumps([])

    return json.dumps(api_controller.search_artist_by_name(request.args['query']))


@blueprint.route('/api/radio/search_genre_by_name')
def search_genre_by_name():
    """
    Ищем жанр по имени.
    """

    if "query" not in request.args:
        return json.dumps([])

    return json.dumps(api_controller.search_genre_by_name(request.args['query']))


@blueprint.route('/api/radio/get_similar_artits/<artist_name>')
def get_similar_artists(artist_name):
    """
    Получаем список связанных исполнителей
    """

    try:
        return json.dumps(api_controller.get_similar_artists(artist_name))
    except Exception as ex:
        logger.exception(u'get_similar_artists error. {0}'.format(ex.message), ex)


@requires_auth
@blueprint.route('/api/radio/rename_personal_station/<station_id>/station_name/<station_name>')
def rename_personal_station(station_id, station_name):
    """
    Переименовать персональную станцию.
    """

    try:
        return json.dumps(api_controller.rename_personal_station(station_id, station_name))
    except Exception as ex:
        logger.exception(u"rename_personal_station error. {0}".format(ex.message), ex)
