# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

from flask import Blueprint, render_template
from controllers_setup import ControllerFactory
from decorators.authenticate import requires_auth, oauth
from utils.web_tools import *


import utils.statistic as statistic
import constants

blueprint = Blueprint('radio', __name__, template_folder='templates')
radio_controller = ControllerFactory.Create(constants.RADIO_CONTROLLER)


@oauth
@blueprint.route('/radio')
def radio():
    return radio_controller.radio()


@blueprint.route('/radio/group/<group_name>')
def radio_group(group_name):
    """
    Если группа не найдена, то генерируется ошибка 404.
    """

    return radio_controller.radio_group(group_name)


@blueprint.route('/radio/station/<station_name>')
@requires_auth
def radio_station(station_name):
    """
    Если станция не найдена, то генерируется ошибка 404.
    """
    statistic.come_on_channel_stat(get_user_id(), station_name)

    return radio_controller.radio_station(station_name)


@blueprint.route('/radio/by/artist/<artist_id>')
def radio_by_artist(artist_id):
    """
    Запустить радио исполнителя
    """
    return radio_controller.radio_by_artist(artist_id)


@blueprint.route('/radio/by/station/<station_id>')
def radio_by_station_id(station_id):
    """
    Запустить персональное радио
    """

    return radio_controller.radio_by_station_id(station_id)


@blueprint.route('/radio/prepare_person_radio')
def prepare_personal_station():
    """
    Настройка персонального радио. Осуществляется выбор исполнителей.
    """

    return radio_controller.prepare_personal_station()


@blueprint.route('/radio/prepare_genre_radio')
def prepare_genre_station():
    """
    Настройка станции по жанрам. Осуществляется выбор жанров.
    """
    return radio_controller.prepare_genre_station()

