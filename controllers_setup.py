# -*- coding: utf-8 -*-

#
# Класс содержит фабрики объектов
#
__author__ = 'Boris'

import constants

from controllers.api import ApiContoller
from controllers.radio import RadioController
from controllers.search import SearchController


_RegisteredControllerList = {
    constants.RADIO_CONTROLLER: RadioController,
    constants.API_CONTROLLER: ApiContoller,
    constants.SEARCH_CONTROLLER: SearchController
}


class ControllerFactory:

    @staticmethod
    def Create(factoryName):

        if not factoryName in _RegisteredControllerList:
            raise KeyError(u"Контроллер с именем '{0}' не найден.".format(factoryName))

        return _RegisteredControllerList[factoryName]()









