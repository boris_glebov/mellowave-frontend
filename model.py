# -*- coding: utf-8 -*-
__author__ = 'Boris'

from mongokit import Document, Connection, ObjectId
import settings
import random
from constants import *

db = Connection(u"{0}:{1}".format(settings.MONGO_SERVER, settings.MONGO_PORT))


class Entity(Document):
    __database__ = settings.MONGO_DATABASE_NAME

    use_dot_notation = True
    use_schemaless = True

    def get_safe(self, key, default=None):
        last_node = self
        for node in key.split('.'):
            try:
                last_node = last_node.get(node)
            except Exception as ex:
                last_node = default
                break

        return default if last_node == self or last_node is None else last_node

    @property
    def id(self):
        return self._id


@db.register
class ArtistEntity(Entity):
    __collection__ = "Artists"

    @property
    def mbid(self):
        return self.get_safe('mbid', None)

    @mbid.setter
    def mbid(self, value):
        self['mbid'] = value

    @property
    def name(self):
        return self.get_safe('name', '')

    @name.setter
    def name(self, value):
        self['name'] = value

    @property
    def tracks(self):
        return self.get_safe('Tracks', [])

    @tracks.setter
    def tracks(self, value):
        self['Tracks'] = value


@db.register
class TrackEntity(Entity):
    __collection__ = "Tracks"

    structure_info = {
        'title': unicode,
        'artist': unicode,
        'vkid': unicode,
        'vk_url': unicode,
        'duration': int,
        'mbid': unicode,
        'file_name': unicode,
        'hash': unicode
    }

    required_fields = ['title', 'artist']

    @staticmethod
    def get_track_info(track):
        if not "track_info" in track or track["track_info"] is None:
            return None

        return db.TrackEntity.find_one({"_id": track["track_info"]})


@db.register
class RadioStationEntity(Entity):
    """
    Радио станция
    """
    __collection__ = "RadioStation"

    structure_info = {
        'name': basestring,
        'group': basestring,
        'notes': basestring,
        'url': basestring,
        'playlists': [{
                          'name': basestring,
                          'path': basestring,
                          'tracks': [{
                                         'vkid': basestring,
                                         'title': basestring,
                                         'artist': basestring,
                                         'vk_url': basestring,
                                         'duration': int,
                                         'track_info': TrackEntity
                                     }]
                      }]
    }

    def set_name(self, value):
        self['name'] = {
            'ru': value,
            'eng': value
        }

    @property
    def station_id(self):
        return self.get_safe('station_id', None)

    @station_id.setter
    def station_id(self, value):
        self['station_id'] = value

    @property
    def station_type(self):
        return self.get_safe('station_type', None)

    @station_type.setter
    def station_type(self, value):
        self['station_type'] = value


@db.register
class RadioStationGroupEntity(Entity):
    """
    Музыкальное направление (Группа)
    """
    __collection__ = "RadioStationGroups"

    structure_info = {
        'group_id': unicode,
        'group_name': {
            'eng': unicode,
            'ru': unicode
        },
        'is_visible': bool,
        'block_color': unicode # Цвет фона блока (?)
    }

    def get_name(self):
        return self["group_name"]["eng"]

    def get_bg_color(self):
        return "bg-color-{0}".format(PERMISSIBLE_COLOR[random.randint(0, len(PERMISSIBLE_COLOR) - 1)])


@db.register
class UserEntity(Entity):
    """
    Пользователь системы
    """
    __collection__ = "Users"

    structure_info = {
        'user_id': unicode,
        'favorite_stations': [{
            'station_id': unicode,
            'name': unicode
        }]
    }

    @staticmethod
    def get_first(user_id):
        try:
            return db.UserEntity.find_one({'user_id': user_id})
        except:
            return None

    @staticmethod
    def exists(user_id):
        return db.UserEntity.find_one({'user_id' : user_id}) is not None


@db.register
class TrackLikeEntity(Entity):
    __collection__ = "TrackLikes"

    structure_info = {
        'user_id': unicode,
        'track_id': unicode,
        'station_id': unicode,
        'is_like': bool,
        'title': unicode,
        'artist': unicode,
    }

    @property
    def user_id(self):
        return self['user_id']

    @user_id.setter
    def user_id(self, value):
        self['user_id'] = value

    @property
    def track_id(self):
        return self['track_id']

    @track_id.setter
    def track_id(self, value):
        self['track_id'] = value

    @property
    def station_id(self):
        return self['station_id']

    @station_id.setter
    def station_id(self, value):
        self['station_id'] = value

    @property
    def is_like(self):
        return self['is_like']

    @is_like.setter
    def is_like(self, value):
        self['is_like'] = value

    @property
    def is_ban(self):
        return self['is_ban']

    @is_ban.setter
    def is_ban(self, value):
        self['is_ban'] = value

    @property
    def title(self):
        return self['title']

    @title.setter
    def title(self, value):
        self['title'] = value

    @property
    def artist(self):
        return self['artist']

    @artist.setter
    def artist(self, value):
        self['artist'] = value


@db.register
class TrackBanEntity(Entity):
    __collection__ = "TrackBans"

    structure_info = {
        '_id': ObjectId,
        'user_id': unicode,
        'track_id': unicode,
        'station_id': unicode,
        'is_ban': bool
    }


@db.register
class StatisticEntity(Entity):
    """
    Коллекция содержит статистические данные
    """

    __collection__ = "Statistics"

    structure_info = {
        '_id': ObjectId,
        'type': unicode,
        'date': unicode,
        'user_id': unicode
    }

@db.register
class KeyValueStoreEntity(Entity):
    """

    """
    __collection__ = "KeyValueStore"

@db.register
class GenreEntity(Entity):
    """
    Коллекция жанров
    """

    __collection__ = "Genres"

    structure_info = {
        '_id': ObjectId,
        'name': unicode,
    }

