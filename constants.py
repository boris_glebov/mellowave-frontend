# -*- coding: utf-8 -*-
__author__ = 'Boris'

#
# В данном файле собраны все константы приложения
#

RADIO_CONTROLLER = "RadioController"
MUSIC_CONTROLLER = "MusicController"
ANIME_CONTROLLER = "AnimeController"
VIDEO_CONTROLLER = "VideoController"
API_CONTROLLER = "ApiController"
SEARCH_CONTROLLER = "SearchController"

#
# Список допустимых цветов
#
PERMISSIBLE_COLOR = ['green',
                     'greenDark',
                     'pink',
                     'pinkDark',
                     'purple',
                     'blue',
                     'blueDark',
                     'orange',
                     'orangeDark',
                     'red']

#
# Типы радио станций
#
class RADIO_STATION_TYPE:
    BROADCAST = 'broadcast' # эфирные станции, например "Наше Радио"
    EXPERT = 'expert' #составляется экспертами deepfm.ru
    AMATEUR = 'amateur'
    PERSONAL = 'personal'
    ARTIST = 'artist',
    GENRE = 'genre',
    FAVORITE = 'favorite'
