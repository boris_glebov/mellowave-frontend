# -*- coding: utf-8 -*-
__author__ = 'Boris'

import telnetlib
import settings

from logger import logger


class LiqudsoapAPI:

    #
    # COMMONS
    #

    def uptime(self):
        template = "uptime"

        return self._invoke(template)

    def list_stations(self):
        template = "list"

        return self._invoke(template)
    #
    # STATIONS
    #

    def autostart(self, station_id):
        template = '{0}(dot)mp3.autostart'

        return self._invoke(template)

    def metadata(self,station_id):
        template = '{0}(dot)mp3.metadata'

        return self._invoke(template)

    def remaining(self, station_id):
        template = '{0}(dot)mp3.remaining'

        return self._invoke(template)

    def skip(self, station_id):
        template = '{0}(dot)mp3.skip'

        return self._invoke(template)

    def start(self, station_id):
        template = '{0}(dot)mp3.start'

        return self._invoke(template)

    def status(self, station_id):
        template = '{0}(dot)mp3.status'

        return self._invoke(template)

    def stop(self, station_id):
        template = '{0}(dot)mp3.stop'.format(station_id)

        return self._invoke(template)

    def next_station(self, station_id):
        template = '{0}(dot)pls.next'.format(station_id)

        return self._invoke(template)

    def reload_station(self, station_id):
        template = '{0}(dot)pls.reload'.format(station_id)

        return self._invoke(template)

    def uri(self, station_id, uri=''):
        template = '{0}(dot)pls.uri {1}'.format(station_id, uri)

        return self._invoke(template)

    def _invoke(self, command):
        try:
            tn = telnetlib.Telnet(settings.LIQUIDSOAP_SERVER, settings.LIQUIDSOAP_PORT)

            tn.write('{0}\n'.format(command))

            response = tn.read_all()

            tn.close()

            return response
        except Exception as ex:
            logger.exception('Invoke liquidsoap command (by telnet) error. {0}'.format(ex.message))

