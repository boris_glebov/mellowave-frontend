# -*- coding: utf-8 -*-
__author__ = 'Glebov Boris'

from flask import session


def get_user():
    return session['user']

def get_user_id():
    user = session['user']

    if user is None:
        return -1

    return user['user_id']