# -*- coding: utf-8 -*-
__author__ = 'Boris'

from datetime import datetime
from model import db

from logger import logger


def come_on_channel_stat(user_id, station_id):
    """
    Пользователь зашел на канал
    """

    statEntity = db.StatisticEntity()

    statEntity['user_id'] = user_id
    statEntity['station_id'] = station_id
    statEntity['type'] = 'come_on_channel_event'
    statEntity['date'] = datetime.now()

    try:
        statEntity.validate()
        statEntity.save()
    except Exception as ex:
        logger.exception(ex.message, ex)


def favorite_track_stat(user_id, station_id, track_id, is_like):
    """
    Пользователь добавил в избранное, либо удалил трэк из изабранного
    """

    statEntity = db.StatisticEntity()

    statEntity['user_id'] = user_id
    statEntity['station_id'] = station_id
    statEntity['track_id'] = track_id
    statEntity['is_like'] = is_like
    statEntity['type'] = 'favorite_track_event'
    statEntity['date'] = datetime.now()

    try:
        statEntity.validate()
        statEntity.save()
    except Exception as ex:
        logger.exception(ex.message, ex)


def block_track_stat(user_id, station_id, track_id, is_ban):
    """
    Пользователь забанил трэк
    """

    statEntity = db.StatisticEntity()

    statEntity['user_id'] = user_id
    statEntity['station_id'] = station_id
    statEntity['track_id'] = track_id
    statEntity['is_ban'] = is_ban
    statEntity['type'] = 'block_track_event'
    statEntity['date'] = datetime.now()

    statEntity['user_id'] = user_id

    try:
        statEntity.validate()
        statEntity.save()
    except Exception as ex:
        logger.exception(ex.message, ex)


def favorite_station_stat(user_id, station_id, is_favorite):
    """
    Пользователь добавил станцию в избранное, либо удалил станцию из избарнных
    """

    statEntity = db.StatisticEntity()

    statEntity['user_id'] = user_id
    statEntity['station_id'] = station_id
    statEntity['type'] = 'favorite_station_event'
    statEntity['is_favorite'] = is_favorite
    statEntity['date'] = datetime.now()

    try:
        statEntity.validate()
        statEntity.save()
    except Exception as ex:
        logger.exception(ex.message, ex)


def add_comment_stat(user_id, station_id, message):
    """
    Пользователь оставил новый комментарий
    """

    statEntity = db.StatisticEntity()

    statEntity['user_id'] = user_id
    statEntity['station_id'] = station_id
    statEntity['type'] = 'add_comment_event'
    statEntity['message'] = message
    statEntity['date'] = datetime.now()

    try:
        statEntity.validate()
        statEntity.save()
    except Exception as ex:
        logger.exception(ex.message, ex)


def track_scrobble_stat(user_id, station_id, track_id, track_name):
    """
    Пользователь прослушал новый трэк
    """

    statEntity = db.StatisticEntity()

    statEntity['user_id'] = user_id
    statEntity['station_id'] = station_id
    statEntity['type'] = 'track_scrobble_event'
    statEntity['track_id'] = track_id
    statEntity['track_name'] = track_name
    statEntity['date'] = datetime.now()

    try:
        statEntity.validate()
        statEntity.save()
    except Exception as ex:
        logger.exception(ex.message, ex)


def person_station_empty_stat(artist_id, artist_name):
    """
    Делаем в базе запись, что не смогли найти трэков по исполнителю
    """

    statEntity = db.StatisticEntity()

    statEntity['type'] = 'person_station_empty'
    statEntity['artist_id'] = artist_id
    statEntity['artist_name'] = artist_name
    statEntity['station_id'] = 'personal'

    try:
        statEntity.validate()
        statEntity.save()
    except Exception as ex:
        logger.exception(ex.message, ex)

#
# Методы для извлечения статистических данных
#


def get_listened_track_count(user_id):
    return db.StatisticEntity.find({'user_id': user_id, 'type': 'track_scrobble_event'}).count()


if __name__ == '__main__':
    print get_listened_track_count(u'6666100')

