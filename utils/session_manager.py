# -*- coding: utf-8 -*-
__author__ = 'Boris'

from model import db


def get_lastfm_key(session):
    """
    Вовзращает ключ для доступа к защищенным методам api last.fm
    """

    if not 'user' in session:
        return None

    if not 'lastfm_session_key' in session['user'] or session['user']['lastfm_session_key'] is None:

        user = db.UserEntity.find_one({'user_id': session['user']['user_id']})

        if user is None:
            return None

        key = user.get_safe('lastfm_session.session.key', None)

        #Сохраняем в кэш:
        session['user']['lastfm_session_key'] = key

        return key

    return session['user']['lastfm_session_key']





