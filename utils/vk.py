# -*- coding: utf-8 -*-
__author__ = 'Boris'

import settings
import hashlib
import vkontakte

from logger import logger


def auth(viewer_id, api_key):
    """
    Выполняет проверку пользователя на строне сервера

    Документация: http://vk.com/page-1_2369267
    """

    md5 = hashlib.md5()
    md5.update('{0}_{1}_{2}'.format(settings.API_ID, viewer_id, settings.API_SECRET))

    return md5.hexdigest() == api_key


def get_profile(uid):
    try:
        vk_client = vkontakte.API(settings.API_ID, settings.API_SECRET)

        profile = vk_client.getProfiles(uid=uid, fields='photo_100,sex')[0]

        return profile
    except Exception as ex:
        logger.info(ex)
        return None



