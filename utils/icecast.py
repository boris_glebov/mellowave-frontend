# -*- coding: utf-8 -*-
__author__ = 'Boris'

from urllib2 import urlopen, Request
from base64 import encodestring
from xml.dom import minidom

import settings
from logger import *


class IcecastStat:
    def __init__(self, username, password, mount):
        self.username = username
        self.password = password
        self.url = settings.ICECAST_ADMIN_URL.format(mount)

    def _get_xml_document(self):
        headers = {
            'Accept': 'application/xml',
            'Authorization': 'Basic %s' % (
                encodestring('%s:%s' % (self.username, self.password))[:-1]
            )
        }

        req = Request(self.url, headers=headers)
        res = urlopen(req)

        return res.read()

    def _get_stats(self):
        """
        Получаем информацию о точке
        """

        doc = minidom.parseString(self._get_xml_document())
        icestats = doc.getElementsByTagName('icestats')[0]

        source = icestats.getElementsByTagName('source')[0]
        data = {
            'server_name': source.getElementsByTagName('server_name')[0].firstChild.data,
            'server_type': source.getElementsByTagName('server_type')[0].firstChild.data,
            'listenurl': source.getElementsByTagName('listenurl')[0].firstChild.data,
            'title': source.getElementsByTagName('title')[0].firstChild.data
        }

        return data

    def get_current_song(self):
        """
        Возвращает название проигрываемого трэка
        """

        try:
            stats = self._get_stats()

            return stats["title"]

        except Exception as ex:
            logger.error('get_current_song', ex.message)
            return ''


if __name__ == '__main__':
    icecast_stat = IcecastStat('admin', 'warava29A', 'adrenaline.mp3')

    print icecast_stat.get_current_song()