# -*- coding: utf-8 -*-
__author__ = 'Boris'

from pyechonest import config, artist, playlist
from model import db
from logger import logger

config.ECHO_NEST_API_KEY = "7HZ6UZEPZH5VKVJ1W"


def get_genres():
    gernres = artist.list_genres()

    return gernres


def get_playlist_by_genres(genres, count=100):
    """
    Формируем плейлист по жанру
    """

    # Лимит 100 трэков
    if count > 100:
        count = 100

    tracks = []

    for genre in genres:
        try:
            result = playlist.static(style=genre, type=u'artist-description', results=count)

            for track in result:
                tracks.append({
                    'artist': track.artist_name,
                    'artist_id': track.artist_id,
                    'mbid': track.id,
                    'name': track.title
                })
        except Exception as ex:
            logger.exception(u'get_playlist_by_genres error. {0}'.format(ex.message))

    return tracks


def get_playlist_by_artists(artists, count=100):
    """
    Формируем плейлист по списку исполнителей
    """

    if count > 100:
        count = 100

    tracks = []

    for a in artists:
        try:
            echonestArtist = artist.Artist(a['name'])

            songs = playlist.static(type='artist-radio', artist_id=[echonestArtist.id], results=count)

            for track in songs:
                tracks.append({
                    'artist': track.artist_name,
                    'artist_id': track.artist_id,
                    'mbid': track.id,
                    'name': track.title
                })
        except Exception as ex:
            logger.exception(u'get_playlist_by_artists. {0}'.format(ex.message))

    return tracks


def generate_search_name():
    pass


def _load_genres():
    genres = get_genres()

    for genre in genres:
        genre_entity = db.GenreEntity()

        genre_entity['name'] = genre['name']

        try:
            genre_entity.save()
            print u'genre "{0}" save.'.format(genre['name'])
        except Exception as ex:
            print ex

    print "END."

if __name__ == '__main__':
    #print get_playlist_by_genres('rock', 1)

    #r = get_playlist_by_genres(['rock', 'pop'], 1)

    r = get_playlist_by_artists([{'name':u'Ozzy Osbourne'}], 10)

    for track in r:
        print u'{0} - {1}'.format(track['artist'], track['name'])