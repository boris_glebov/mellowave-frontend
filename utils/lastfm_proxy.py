# -*- coding: utf-8 -*-
__author__ = 'Boris'

import utils.lastfmapi as lastapi
import settings
import hashlib
import time

from datetime import datetime


last_api = lastapi.LastFmApi(settings.LASTFM_API_KEY)


def getArtistInfo(artist_name):
    artist_info = last_api.artist_getInfo(artist=artist_name, format='json', lang='ru')

    if artist_info is not None:
        content = artist_info['artist']['bio']['content']\
            .replace('User-contributed text is available under the Creative Commons By-SA License and may also be available under the GNU FDL.', ' ')

        artist_info['artist']['bio']['content'] = content

    return artist_info


def getSimilarsArtsits(artist_name):
    similars = last_api.artist_getSimilar(artist=artist_name, format='json', lang='ru')

    return similars


def getSession(token):
    session = last_api.auth_getSession(token=token,
                                       api_key=settings.LASTFM_API_KEY,
                                       api_sig=_calcApiSig({
                                           'api_key': settings.LASTFM_API_KEY,
                                           'method': u'auth.getSession',
                                           'token': token,
                                       }))

    return session


def loveTrack(api_session, track, artist):
    resp = last_api.track_love(track=track,
                               artist=artist,
                               api_key=settings.LASTFM_API_KEY,
                               api_sig=_calcApiSig({
                                    'api_key': settings.LASTFM_API_KEY,
                                    'method': u'track.love',
                                    'track': track,
                                    'artist': artist,
                                    'sk': api_session
                               }),
                               sk=api_session)

    return 'status' in resp and resp['status'] == 'ok'


def unloveTrack(api_session, track, artist):
    resp = last_api.track_unlove(track=track,
                                 artist=artist,
                                 api_key=settings.LASTFM_API_KEY,
                                 api_sig=_calcApiSig({
                                       'api_key': settings.LASTFM_API_KEY,
                                       'method': u'track.unlove',
                                       'track': track,
                                       'artist': artist,
                                       'sk': api_session
                                 }),
                                 sk=api_session)

    return 'status' in resp and resp['status'] == 'ok'


def scrobbleTrack(api_session, track, artist):

    timestamp = u'{0}'.format(int(time.mktime(datetime.now().timetuple())))

    resp = last_api.track_scrobble(track=track,
                                   artist=artist,
                                   timestamp=timestamp,
                                   api_key=settings.LASTFM_API_KEY,
                                   api_sig=_calcApiSig({
                                        'track': track,
                                        'artist': artist,
                                        'timestamp': timestamp,
                                        'method': u'track.scrobble',
                                        'api_key': settings.LASTFM_API_KEY,
                                        'sk': api_session
                                   }),
                                   sk=api_session)

    return resp


def _calcApiSig(params):

    md5 = hashlib.md5()

    keys = params.keys()
    keys.sort()

    api_sig = u''
    for key in keys:
        api_sig += u'{0}{1}'.format(key, params[key])

    md5.update(u'{0}{1}'.format(api_sig, settings.LASTFM_API_SECRET).encode('utf-8'))

    return md5.hexdigest()




if __name__ == '__main__':
    artist = getArtistInfo(u'Король и Шут')

    print artist



