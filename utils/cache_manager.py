# -*- coding: utf-8 -*-
__author__ = 'Boris'

from model import db
from datetime import datetime

def get_history_tracks(station_id, cache):
    """
    Получить историю трэков из кэша
    """

    return cache.get('{0}_tracks_cache'.format(station_id))


def get_listeners(station_id, cache):
    """
    Получить список пользователь, которые сейчас прослушивают канал
    """

    key = '{0}_listeners'.format(station_id)
    return cache.get(key)


def get_who_online(cache):
    """
    Получить всех пользователей online
    """

    key = 'who_online_cache'

    return cache.get(key)


def get_person_station(user_id, station_id, cache):
    """
    Возвращает станцию исполнителя.
    """

    key = '{0}_{1}_person_station'.format(user_id, station_id)

    station = db.KeyValueStoreEntity.find_one({'key': key})

    return station["value"] if station is not None else None


def get_person_station_track_number(user_id, station_id, cache):
    """
    Вовзращает номер текущего трэка
    """

    key = '{0}_{1}_person_station_track_number'.format(user_id, station_id)

    return cache.get(key)


def get_artist_bio(artist_id, cache):
    """
    Получить биографию артиста
    """

    key = u'{0}_bio'.format(artist_id)

    return cache.get(key)



def set_listenres(station_id, listeners, cache):
    """
    Сохраняем обновленный список слушателей
    """

    key = '{0}_listeners'.format(station_id)
    cache.set(key, listeners)


def set_who_online(listeners, cache):
    """
    Получить список тек, кто онлайн
    """

    key = 'who_online_cache'

    cache.set(key, listeners)


def set_person_station(user_id, station_id, artist_station, cache):
    """
    Сохраняем станцию в кэш
    """

    key = '{0}_{1}_person_station'.format(user_id, station_id)

    item = db.KeyValueStoreEntity.find_one({'key': key})

    if item is None:
        item = db.KeyValueStoreEntity()
        item['key'] = key
        item['last_update'] = datetime.now()

    item['value'] = artist_station

    try:
        item.save()
    except Exception as ex:
        pass

    return key


def set_person_station_track_number(user_id, station_id, value, cache):
    """
    Устанавливаем номер играющего трэка
    """

    key = '{0}_{1}_person_station_track_number'.format(user_id, station_id)

    cache.set(key, value, timeout=24 * 60)


def set_artist_bio(artist_id, bio, cache):
    """
    Биография исполнителя
    """

    key = u'{0}_bio'.format(artist_id)

    cache.set(key, bio)

